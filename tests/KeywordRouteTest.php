<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class KeywordRouteTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testInsertKeyword()
    {
        $this->post('/api/keyword', [
            'keyword' => 'amazing garclic',
            'api_token' => '123456_abcdef'
        ])
            ->seeJson([
                'keyword' => 'amazing garclic',
            ]);

        //2nd time should not add the same 'keyword'
        $this->post('/api/keyword', [
            'keyword' => 'amazing garclic',
            'api_token' => '123456_abcdef'
        ])
            ->seeJson([
                'status' => false,
            ]);


        $this->seeInDatabase('amazon_keyword_list', ['keyword' => 'amazing garclic']);


        \App\Models\AmazonKeywordList::where('keyword', 'amazing garclic')->delete();
    }
}
