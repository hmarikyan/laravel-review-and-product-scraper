<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AsinRouteTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testInsertAsin()
    {

        $this->post('/api/asin', [
            'asin' => 'B00ZDR7CUG',
            'api_token' => '123456_abcdef'
        ])
        ->seeJson([
            'asin' => 'B00ZDR7CUG',
        ]);

        //2nd time should not add the same 'asin'
        $this->post('/api/asin', [
            'asin' => 'B00ZDR7CUG',
            'api_token' => '123456_abcdef'
        ])
            ->seeJson([
                'status' => false,
            ]);


        $this->seeInDatabase('amazon_asin_list', ['asin' => 'B00ZDR7CUG']);


        \App\Models\AmazonAsinList::where('asin', 'B00ZDR7CUG')->delete();
    }
}
