<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

use App\Packages\Utils\ASINManager;

class FillAsins extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:fill_asins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read data from amazon_products, group and insert into amazon_asin_list.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 600);

        $this->comment("Uploading Asin list...");

        ASINManager::groupAndFill();

        $this->comment("Finish.");
    }
}
