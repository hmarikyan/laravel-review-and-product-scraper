<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;


use App\Models\AmazonAsinList;
use App\Packages\ReviewScrapeReadyCheck;

use App\Jobs\ReviewsPageScrapeAllJob;
use App\Jobs\ReviewsPageScrapeRecentJob;

class DispatchReviewScrapeJobs extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:amazon_review';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatcher for amazon reviews';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment("Starting REVIEW dispatcher");

        $asin_list = AmazonAsinList::all();
        if(count($asin_list) == 0){
            $this->info("There is NO any asin in db");
        }else {

            foreach ($asin_list as $asin) {
                $status = ReviewScrapeReadyCheck::check($asin);

                switch ($status) {
                    case ReviewScrapeReadyCheck::ALL: {
                        $this->comment("Add ALL review scrape into queue. ASIN:". $asin->asin);

                        $job = (new ReviewsPageScrapeAllJob($asin->asin))->onQueue('amazon_scrape_reviews');
                        $this->dispatch($job);

                        break;
                    }
                    case ReviewScrapeReadyCheck::RECENT: {
                        $this->comment("Add RECENT review scrape into queue. ASIN:". $asin->asin);

                        $job = (new ReviewsPageScrapeRecentJob($asin->asin))->onQueue('amazon_scrape_reviews');
                        $this->dispatch($job);

                        break;
                    }
                    default : {

                        break;
                    }

                }
            }

        }

        $this->comment("Finish");
    }
}
