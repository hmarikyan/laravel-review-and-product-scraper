<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

use App\Models\AmazonKeywordList;
use App\Packages\ProductScrapeReadyCheck;

use App\Jobs\SearchResultsPageScrapeJob;

class DispatchProductScrapeJobs extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:amazon_product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatcher for amazon products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment("Starting Product dispatcher");

        $k_list = AmazonKeywordList::all();
        if(count($k_list) == 0){
            $this->info("There is NO any keyword in db");
        }else {

            foreach ($k_list as $k) {
                $status = ProductScrapeReadyCheck::check($k);

                if($status == true){
                    $this->comment("Add product scrape into queue. Keyword:". $k->keyword);

                    $job = (new SearchResultsPageScrapeJob($k->keyword))->onQueue('amazon_search_results_scrape');
                    $this->dispatch($job);
                }else{

                }
            }

        }

        $this->comment("Finish");
    }
}
