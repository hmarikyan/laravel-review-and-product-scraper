<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

use App\Models\AmazonKeywordList;
use App\Packages\Utils\CSVKeywordReader;

class UploadKeywords extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:upload_keywords';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read keywords from csv file and insert to amazon_keyword_list table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 600);

        $this->comment("Uploading keywords...");

        $keywords = CSVKeywordReader::getKeywordsCSV();

        foreach($keywords as $kw){
            $model = new AmazonKeywordList();
            $model->keyword = $kw;
            $model->save();
        }

        $this->comment("Finish !. Uploaded ". count($keywords). " keywords.");
    }
}
