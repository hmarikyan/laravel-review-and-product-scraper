<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\DispatchReviewScrapeJobs::class,
        \App\Console\Commands\DispatchProductScrapeJobs::class,
        \App\Console\Commands\UploadKeywords::class,
        \App\Console\Commands\FillAsins::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('inspire')->hourly();
        $schedule->command('scrapper:amazon_review')->dailyAt('03:00');
        $schedule->command('scrapper:amazon_product')->dailyAt('03:00');
    }
}
