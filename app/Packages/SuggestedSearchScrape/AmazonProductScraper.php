<?php

namespace App\Packages\SuggestedSearchScrape;

use Exception;
use DOMXPath;
use DOMDocument;

use App\Packages\SuggestedSearchScrape\SuggestedSearchScrapeInterface;
use App\Packages\simple_html_dom;

use App\Packages\ScrapeLayouts\AmazonProduct\AmazonProductLayout1;
use App\Packages\ScrapeLayouts\AmazonSponsoredProduct\AmazonSponsoredProductLayout1;
use App\Packages\ScrapeLayouts\AmazonSponsoredProduct\AmazonSponsoredProductLayout2;
use App\Packages\ScrapeLayouts\AmazonProductPage\AmazonProductPageLayout1;
use App\Packages\ScrapeLayouts\AmazonAd\AmazonAdLayout1;
use App\Packages\ScrapeLayouts\AmazonAd\AmazonAdLayout2;

use App\Packages\Repositories\RepositoryInterface;
use App\Packages\Repositories\AmazonProductRepository;

use App\Traits\ScrapeTrait;


/**
 * Class AmazonProductScraper
 * @package App\Packages
 */
class AmazonProductScraper implements SuggestedSearchScrapeInterface {

    use ScrapeTrait;

    /**
     * parsed html data
     * @var \App\Packages\simple_html_dom|bool|string
     */
    public $html = "";

    /**
     * @var
     */
    protected $repo = null;


    public $error = null;
    protected $source;
    protected $suggestedKeywordArray;

    public $url;

    /*DATA */
    public $product_page = array();
    public $ad = array();
    public $products = array();
    public $sponsored_products = array();

    /**
     * Takes the raw source of a suggested search result and saves it
     * inside the class for further processing
     *
     * @param $source
     */
    public function __construct(RepositoryInterface $repo = null, $source){

        try{
            $source = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $source);
            $this->source = $source;
            $this->html = simple_html_dom::str_get_html($source);

        }catch(Exception $e){
           echo  $this->error = $e->getMessage();
        }

        //Instantinate repository
        if(empty($repo)){
            $this->repo = new AmazonProductRepository();
        }else{
            $this->repo = $repo;
        }
    }

    /**
     * Setter
     */
    public function set_url($url){
        $this->url = $url;
    }

    /**
     * FETCH PRODUCTS PAGE
     */
    public function get_page(){

        $product_page = array();
        $classes = array(
            AmazonProductPageLayout1::class,
        );

        do{
            $i = isset($i) ? ++$i : 0;
            if(isset($classes[$i])){
                $class = $classes[$i];
            }else{
                break;
            }
            $product_page = $class::getData($this->html);
        }
        while(empty($product_page));

        return $this->product_page = $product_page;
    }

    /**
     * @param $keywords
     */
    public function set_seed_keywords($keywords){
        $this->product_page['seed_keywords'] = $keywords;
        return $keywords;
    }

    /**
     * HETCH Advertize
     */
    public function get_ad(){
        $ad = array();

        $classes = array(
            AmazonAdLayout1::class,
            AmazonAdLayout2::class,
        );

        do{
            $i = isset($i) ? ++$i : 0;
            if(isset($classes[$i])){
                $class = $classes[$i];
            }else{
                break;
            }
            $ad = $class::getData($this->html);
        }
        while(empty($ad));

        if(!$ad) $ad = array();
        return $this->ad = $ad;
    }

    //get products
    public function get_products()
    {
        $products = array();
        $classes = array(
            AmazonProductLayout1::class,
        );

        do{
            $i = isset($i) ? ++$i : 0;
            if(isset($classes[$i])){
                $class = $classes[$i];
            }else{
                break;
            }
            $products = $class::getData($this->html);
        }
        while(empty($products));

        if(!$products) $products = array();
        return $this->products = $products;
    }

    //
    public function get_sponsored_products(){
        $products = array();
        $classes = array(
            AmazonSponsoredProductLayout1::class,
            AmazonSponsoredProductLayout2::class,
        );

        do{
            $i = isset($i) ? ++$i : 0;
            if(isset($classes[$i])){
                $class = $classes[$i];
            }else{
                break;
            }
            $products = $class::getData($this->html);
        }
        while(empty($products));

        if(!$products) $products = array();
        return $this->sponsored_products = $products;
    }

    /**
     * Extracts the relevant data from the saved source,
     * creates a new model, and populates it with the
     * required data
     *
     * @return $this
     */
    public function prepare(){
        $this->get_page();
        $this->get_products();
        $this->get_sponsored_products();
        $this->get_ad();

        return $this;
    }

    /**
     * Saves the model created in the prepare() method
     *
     * @return $this
     */
    public function save(){
        try{
            $save_data = array(
                'product_page' => $this->product_page,
                'ad' => $this->ad,
                'products' => array_merge($this->products, $this->sponsored_products)
            );

            /*save data into repo*/
            $res = $this->repo->create($save_data);

            if($res == false){
                throw new Exception($this->repo->getError());
            }

            return $save_data;

        } catch(Exception $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}