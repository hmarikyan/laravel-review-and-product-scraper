<?php

namespace App\Packages\SuggestedSearchScrape;

use Exception;

use App\Packages\SuggestedSearchScrape\SuggestedSearchScrapeInterface;
use App\Packages\simple_html_dom;

use App\Packages\Repositories\RepositoryInterface;
use App\Packages\Repositories\AmazonReviewRepository;

use App\Traits\ScrapeTrait;

use App\Packages\ScrapeLayouts\AmazonReviewPage\AmazonReviewPageLayout1;
use App\Packages\ScrapeLayouts\AmazonRating\AmazonRatingLayout1;
use App\Packages\ScrapeLayouts\AmazonReview\AmazonReviewLayout1;

/**
 * Class AmazonReviewScraper
 * @package App\Packages
 */
class AmazonReviewScraper implements SuggestedSearchScrapeInterface {

    use ScrapeTrait;

    /**
     * parsed html data
     * @var \App\Packages\simple_html_dom|bool|string
     */
    public $html = "";

    /**
     * @var
     */
    protected $repo = null;

    public $error = null;

    public $url;

    /*DATA */
    public $review_page = array();
    public $ratings = array();
    public $reviews = array();
    public $positive_review = array();
    public $critical_review = array();

    public $next_page_url = null;

    /**
     * Takes the raw source of a suggested search result and saves it
     * inside the class for further processing
     *
     * @param $source
     */
    public function __construct(RepositoryInterface $repo = null, $source){

        try{
            $this->html = simple_html_dom::str_get_html($source);

        }catch(Exception $e){
            $this->error = $e->getMessage();
        }

        //Instantinate repository
        if(empty($repo)){
            $this->repo = new AmazonReviewRepository();
        }else{
            $this->repo = $repo;
        }
    }

    /**
     * Setter
     */
    public function set_url($url){
        $this->url = $url;
    }

    /**
     * FETCH PERCANAGE
     */
    public function get_ratings(){
        $ratings = array();
        $classes = array(
            AmazonRatingLayout1::class,
        );

        do{
            $i = isset($i) ? ++$i : 0;
            if(isset($classes[$i])){
                $class = $classes[$i];
            }else{
                break;
            }
            $ratings = $class::getData($this->html);
        }
        while(empty($ratings));

        if(!$ratings) $ratings = array();
        return $this->ratings = $ratings;
    }

    /**
     * HETCH PAGE DATA
     */
    public function get_review_page(){
        $review_page = array();

        $classes = array(
            AmazonReviewPageLayout1::class,
        );

        do{
            $i = isset($i) ? ++$i : 0;
            if(isset($classes[$i])){
                $class = $classes[$i];
            }else{
                break;
            }
            $review_page = $class::getData($this->html);
        }
        while(empty($review_page));

        if(empty($review_page))
            return array();

        $this->next_page_url = $review_page['next_page_url'];
        unset($review_page['next_page_url']);

        $this->review_page = $review_page;
        $this->review_page['url'] = $this->url;
        $this->review_page['is_next_page'] = $this->next_page_available();

        return $this->review_page;
    }

    /**
     * NEXT PAGE
     */
    public function next_page_available(){
        return !empty($this->next_page_url) ? true : false;
    }


    /**
     * FETCH POSITIVE REVIEW
     */
    public function get_positive_review(){
        $rev = array();
        $classes = array(
            AmazonReviewLayout1::class,
        );

        do{
            $i = isset($i) ? ++$i : 0;
            if(isset($classes[$i])){
                $class = $classes[$i];
            }else{
                break;
            }
            $rev = $class::getData($this->html)->getPositiveReview();
        }
        while(empty($rev));

        if(!$rev) $rev = array();
        return $this->positive_review = $rev;

    }

    /**
     * FETCH POSITIVE REVIEW
     */
    public function get_critical_review(){
        $rev = array();
        $classes = array(
            AmazonReviewLayout1::class,
        );

        do{
            $i = isset($i) ? ++$i : 0;
            if(isset($classes[$i])){
                $class = $classes[$i];
            }else{
                break;
            }
            $rev = $class::getData($this->html)->getCriticalReview();
        }
        while(empty($rev));

        if(!$rev) $rev = array();
        return $this->critical_review = $rev;
    }

    // get review list
    public function get_normal_reviews(){
        $rev = array();
        $classes = array(
            AmazonReviewLayout1::class,
        );

        do{
            $i = isset($i) ? ++$i : 0;
            if(isset($classes[$i])){
                $class = $classes[$i];
            }else{
                break;
            }
            $rev = $class::getData($this->html)->getNormalReviews();
        }
        while(empty($rev));

        if(!$rev) $rev = array();
        return $this->reviews = $rev;
    }

    /**
     * Extracts the relevant data from the saved source,
     * creates a new model, and populates it with the
     * required data
     *
     * @return $this
     */
    public function prepare(){

        try{
            //prepare all data to save into db
            $this->get_ratings();
            $this->get_review_page();
            $this->get_positive_review();
            $this->get_critical_review();
            $this->get_normal_reviews();

        }catch(\Exception $e){

        }

        return $this;
    }

    /**
     * Saves the model created in the prepare() method
     *
     * @return $this
     */
    public function save(){

        try{
            /*prepare data*/
            if(!empty($this->positive_review)){
                $this->reviews[] = $this->positive_review;
            }
            if(!empty($this->critical_review)){
                $this->reviews[] = $this->critical_review;
            }

            $save_data = array(
                'review_page'=>$this->review_page,
                'ratings'=>$this->ratings,
                'reviews'=>$this->reviews
            );

            /*save data into repo*/
            $res = $this->repo->create($save_data);
            $save_data['review_page'] = $res;

            if($res == false){
                throw new Exception($this->repo->getError());
            }

            return $save_data;

        }catch(Exception $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}