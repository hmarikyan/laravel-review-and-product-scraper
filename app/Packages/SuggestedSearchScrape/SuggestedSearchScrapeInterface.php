<?php

namespace App\Packages\SuggestedSearchScrape;

use App\Packages\Repositories\RepositoryInterface;

interface SuggestedSearchScrapeInterface {

    /**
     * Takes the raw source of a suggested search result and saves it
     * inside the class for further processing
     *
     * @param $source
     */
    public function __construct(RepositoryInterface $repo, $source);

    /**
     * Extracts the relevant data from the saved source,
     * creates a new model, and populates it with the
     * required data
     *
     * @return $this
     */
    public function prepare();

    /**
     * Saves the model created in the prepare() method
     *
     * @return $this
     */
    public function save();
}