<?php

namespace App\Packages\ScrapeLayouts\AmazonProduct;

/**
 * Class AmazonProductAbstract
 * @package App\Packages\ScrapeLayouts\AmazonProduct
 */
abstract class AmazonProductAbstract
{
    protected $html;
    protected $htmlProducts;

    protected $products = array();
    protected $product = array();

    public function __construct($html){
        $this->html = $html;
    }

    abstract public function getProductsSection();
    abstract public function getProducts();

    abstract public function getAsin($pr);
    abstract public function getIsSponsored($pr);
    abstract public function getTitle($pr);
    abstract public function getBrand($pr);
    abstract public function getCurrentPrice($pr);
    abstract public function getOriginalPrice($pr);
    abstract public function getIsPrime($pr);
    abstract public function getReviewAvg($pr);
    abstract public function getReviewCount($pr);
    abstract public function getBestSeller($pr);
    abstract public function getRank($pr);
    abstract public function getPromotionsCount($pr);
    abstract public function getPromotionsDetails($pr);
    abstract public function getOffers($pr);
    abstract public function getVariations($pr);
    public function getType(){
        $this->product['type'] = "normal";
    }

    abstract public function checkFields();
}