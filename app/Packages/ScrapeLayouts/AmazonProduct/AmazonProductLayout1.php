<?php
namespace App\Packages\ScrapeLayouts\AmazonProduct;

use Exception;

use App\Packages\simple_html_dom;
use App\Packages\ScrapeLayouts\AmazonProduct\AmazonProductAbstract;
use App\Packages\ScrapeLayouts\LayoutInterface;

/**
 * Class AmazonProductLayout1
 * @package App\Packages\ScrapeLayouts\AmazonProduct
 */
class AmazonProductLayout1 extends AmazonProductAbstract implements LayoutInterface
{
    public function __construct($html){
        parent::__construct($html);
    }

    /**
     * PRODUCTS SECTION in HTML
     */
    public function getProductsSection(){
        $this->htmlProducts = $this->html->find(".s-grid-view li.s-result-item");
        if(empty($this->htmlProducts )){
            $this->htmlProducts  = $this->html->find("li.s-result-item");
        }
    }

    /**
     * ITERATION
     */
    public function getProducts(){
        $this->products = array();

        foreach($this->htmlProducts as $pr){
            $this->product = array();

            $this->getType();
            $this->getAsin($pr);
            $this->getIsSponsored($pr);
            $this->getTitle($pr);
            $this->getBrand($pr);
            $this->getCurrentPrice($pr);
            $this->getOriginalPrice($pr);
            $this->getIsPrime($pr);
            $this->getRank($pr);
            $this->getPromotionsCount($pr);
            $this->getPromotionsDetails($pr);
            $this->getOffers($pr);
            $this->getVariations($pr);
            $this->getBestSeller($pr);
            $this->getReviewAvg($pr);
            $this->getReviewCount($pr);

            if($this->checkFields())
                $this->products[] = $this->product;
            //else continue;
        }
    }



    public function getAsin($pr){
        $this->product['asin'] = $pr->getAttribute("data-asin");
    }

    public function getIsSponsored($pr){
        $this->product['is_sponsored'] = 0;
    }

    public function getTitle($pr){
        $title = $pr->find(".s-access-detail-page",0);
        $this->product['title'] = !empty($title->plaintext)? $title->plaintext: "";
    }

    public function getBrand($pr){
        $this->product['brand'] = '';

        $brand = $pr->find(".s-access-detail-page",0);
        if(!empty($brand) ) {
            if (!empty($brand->next_sibling()) && strpos($brand->next_sibling()->plaintext, "by ") !== false) {
                $brand = $brand->next_sibling();
            }elseif (strpos($brand->parent()->last_child(), "by ") !== false) {
                $brand = $brand->parent()->last_child();
            }elseif (strpos($brand->parent()->next_sibling(), "by ") !== false) {
                $brand = $brand->parent()->next_sibling();
            }else{
                $brand = "";
            }

            $brand = !empty($brand->plaintext) ? $brand->plaintext : "";
            $this->product['brand'] = str_replace('by ', '', $brand);
        }
    }

    public function getCurrentPrice($pr){
        $current_price = $pr->find(".s-price",0);
        $current_price = !empty($current_price->innertext)? $current_price->innertext: "";

        $this->product['current_price'] = str_replace("$","", $current_price);
    }

    public function getOriginalPrice($pr){
        $original_price = $pr->find(".a-text-strike",0);
        $original_price = !empty($original_price->innertext)? $original_price->innertext: "";
        $this->product['original_price'] = str_replace("$","", $original_price);
    }

    public function getIsPrime($pr){
        $is_prime = $pr->find(".a-icon-prime",0);
        $this->product['is_prime'] = !empty($is_prime)? 1 : 0;
    }

    public function getReviewAvg($pr){
        $this->product['review_avg'] = 0;

        $review_avg = $pr->find(".a-icon-star", 0 );
        $review_avg = !empty($review_avg) ? $review_avg->plaintext : '' ;
        if(!empty($review_avg)){
            $review_avg = explode(' ', $review_avg);
            $this->product['review_avg'] = $review_avg[0];
        }
    }

    public function getReviewCount($pr){
        $this->product['review_count'] = 0;

        $review_count = $pr->find(".a-icon-star", 0 );
        if(!empty($review_count)){
            $review_count = $review_count->parent()->parent()->parent()->next_sibling();
            $review_count = !empty($review_count) ? $review_count->plaintext : '';
            $this->product['review_count'] = str_replace(",", "", $review_count);
        }

    }

    public function getBestSeller($pr){
        $best_seller = $pr->find(".sx-bestseller", 0 );
        $this->product['best_seller'] = !empty($best_seller) ? $best_seller->plaintext : '';
    }

    public function getRank($pr){
        $rank = $pr->find(".s-access-detail-page", 0 );
        preg_match("/sr_1_(\d*)/", @$rank->href, $results);
        $this->product['rank'] = !empty($results[1])? $results[1] : 0;
    }

    public function getPromotionsCount($pr){
        $this->product['promotions'] = 0;
        $promote = 0;

        $promotions_details = $pr->find(".a-popover-preload ul", 0 );
        if(!empty($promotions_details)){
            $promotions_count = $pr->find(".a-declarative .a-popover-trigger");
            foreach($promotions_count as $pc){
                if (strpos($pc->plaintext, "more promotion") !== false ) {
                    $promote = $pc->plaintext;
                    break;
                }
            }
            $this->product['promotions'] = intval($promote);
        }
    }

    public function getPromotionsDetails($pr){
        $this->product['promotions_details'] = "";

        $promotions_details = $pr->find(".a-popover-preload ul", 0 );
        if(!empty($promotions_details)){
            $promotions_details = simple_html_dom::str_get_html($promotions_details);

            $_promotions_details = $promotions_details->find("li");
            if(!empty($_promotions_details)) {
                foreach ($_promotions_details as $_pd) {
                    $this->product['promotions_details'] .= $_pd->plaintext. ", ";
                }
            }
        }
    }

    public function getOffers($pr){
        $this->product['offers'] = "";
        $result = "";

        try{
            //find offers block
            $offers = $pr->find("table.a-normal",0);
            if(!empty($offers)){
                $of = simple_html_dom::str_get_html($offers);
                $of = $of->find("tr");
                foreach($of as $_of){
                    $result = trim($_of->plaintext).", ";
                }
            }else{
                $offers = $pr->find(".s-price",0);
                if(empty($offers))
                    throw new Exception("no offer");
                $of = $offers->parent()->parent();

                $of_flag = 0;
                $text = $of->plaintext;
                while($text != "More Buying Choices"){
                    $of  = $of->next_sibling();

                    if(empty($of))
                        throw new Exception("no offer");

                    $text = $of->plaintext;
                    if (strpos($text, "More Buying Choices") !== false ) {
                        $of_flag = 1;
                        break;
                    }
                }

                if($of_flag == 0)
                    throw new Exception("no offer");

                //fetch each offer
                $of_sib =  $of->next_sibling();
                if(strpos($of_sib, "offer") !== false){
                    while(!empty($of_sib)){
                        $result .= trim($of_sib->plaintext). ", ";
                        $of_sib =  $of_sib->next_sibling();
                    }
                }elseif(strpos( $of->plaintext, "offer") !== false){
                    $offers = $of->find(".a-row");
                    foreach($offers as $offer){
                        if(empty($offer->plaintext) || strpos( $offer->plaintext, "More Buying Choices") !== false)
                            continue;

                        $result .= trim($offer->plaintext). ", ";
                    }
                }
            }
        }catch(Exception $e){
            $this->product['offers'] = "";
        }

        $this->product['offers'] = $result;
    }

    public function getVariations($pr){
        $this->product['variations'] = '';

        if($variations = $pr->find(".a-span12",0)){
            $variations = $pr->find(".a-span12",0)->children(0)->children(1);
        }else{

        }

        if(!empty($variations)){
            $this->product['variations'] = trim($variations->plaintext);
        }
    }

    /**
     * product fields
     * @return bool
     */
    public function checkFields(){
        if(empty($this->product['asin']) || empty($this->product['title']) || empty($this->product['current_price']))
            return false;

        return true;
    }

    /**
     * MAIN FUNCTION
     * @param $html
     * @return array|bool
     */
    public static function getData($html){
        $ob = new self($html);
        $ob->getProductsSection();

        if(empty($ob->htmlProducts))
            return false;

        $ob->getProducts();

        if(empty($ob->products))
            return false;

        return $ob->products;
    }
}