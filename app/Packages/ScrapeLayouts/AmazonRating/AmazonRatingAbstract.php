<?php

namespace App\Packages\ScrapeLayouts\AmazonRating;

/**
 * Class AmazonRatingAbstract
 * @package App\Packages\ScrapeLayouts\AmazonRating
 */
abstract class AmazonRatingAbstract
{
    protected $html;
    protected $htmlRatings;

    protected $ratings = array();
    protected $rating = array();

    public function __construct($html){
        $this->html = $html;
    }

    abstract public function getRatingsSection();
    abstract public function getRatings();

    abstract public function getPercentage($rt);
    abstract public function getStar($rt);

    abstract public function checkFields();
}