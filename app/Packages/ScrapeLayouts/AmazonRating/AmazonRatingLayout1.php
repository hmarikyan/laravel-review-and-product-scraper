<?php
namespace App\Packages\ScrapeLayouts\AmazonRating;

use Exception;

use App\Packages\simple_html_dom;
use App\Packages\ScrapeLayouts\AmazonRating\AmazonRatingAbstract;
use App\Packages\ScrapeLayouts\LayoutInterface;

/**
 * Class AmazonRatingLayout1
 * @package App\Packages\ScrapeLayouts\AmazonRating
 */
class AmazonRatingLayout1 extends AmazonRatingAbstract implements LayoutInterface
{
    public function __construct($html){
        parent::__construct($html);
    }

    /**
     * Html SECTION
     */
    public function getRatingsSection(){
        $this->htmlRatings = $this->html->find("tr.a-histogram-row");
    }

    /**
     * ITERATION
     */
    public function getRatings(){
        $this->ratings = array();

        foreach($this->htmlRatings as $rt){
            $this->rating = array();

            $this->getPercentage($rt);
            $this->getStar($rt);


            if($this->checkFields())
                $this->ratings[] = $this->rating;
            //else continue;
        }
    }


    public function getPercentage($rt){
        $percentage =  $rt->find(".histogram-review-count",0);
        $percentage = !empty($percentage->innertext) ? $percentage->innertext : "0";
        $percentage = str_replace( '%', '', $percentage );

        $this->rating['percentage'] = $percentage;
    }

    public function getStar($rt){
        $star = $rt->find("td", 0);
        $star = !empty($star->plaintext) ? $star->plaintext : "";
        $star = intval($star);

        $this->rating['star'] = $star;
    }


    /**
     * fields
     * @return bool
     */
    public function checkFields(){
        if(empty($this->rating['percentage']) || empty($this->rating['star']))
            return false;

        return true;
    }

    /**
     * MAIN FUNCTION
     * @param $html
     * @return array|bool
     */
    public static function getData($html){
        $ob = new self($html);
        $ob->getRatingsSection();

        if(empty($ob->htmlRatings))
            return false;

        $ob->getRatings();

        if(empty($ob->ratings))
            return false;

        return $ob->ratings;
    }
}