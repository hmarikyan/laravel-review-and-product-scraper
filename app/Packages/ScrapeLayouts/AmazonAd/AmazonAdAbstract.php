<?php

namespace App\Packages\ScrapeLayouts\AmazonAd;

/**
 * Class AmazonAdAbstract
 * @package App\Packages\ScrapeLayouts\AmazonAd
 */
abstract class AmazonAdAbstract
{
    protected $html;
    protected $htmlAd;

    protected $ad = array();

    public function __construct($html){
        $this->html = $html;
    }

    abstract public function getLink();
    abstract public function getTitle();
    abstract public function getBrand();

    abstract public function checkFields();
}