<?php
namespace App\Packages\ScrapeLayouts\AmazonAd;

use Exception;

use App\Packages\simple_html_dom;
use App\Packages\ScrapeLayouts\AmazonAd\AmazonAdAbstract;
use App\Packages\ScrapeLayouts\LayoutInterface;

/**
 * Class AmazonAdLayout1
 * @package App\Packages\ScrapeLayouts\AmazonAd
 */
class AmazonAdLayout1 extends AmazonAdAbstract implements LayoutInterface
{
    public function __construct($html){
        parent::__construct($html);
    }

    public function getAdSection(){
        $this->htmlAd = $this->html->find(".amsSparkleAdWrapper",0 );
    }



    public function getLink(){
        $link = $this->htmlAd->find("a.amsSparkleClickableArea",0);
        $this->ad['link'] = !empty($link->href)? $link->href: "";
    }

    public function getTitle(){
        $title = $this->htmlAd->find(".amsSparkleHead",0);
        $this->ad['title'] = !empty($title->innertext)? $title->innertext: "";
    }

    public function getBrand(){
        $brand = $this->htmlAd->find(".amsSparkleBrandName",0);
        $brand = !empty($brand->innertext)? $brand->innertext: "";
        $this->ad['brand'] = str_replace("Sponsored by ",'', $brand);
    }


    /**
     * check product page fields
     * @return bool
     */
    public function checkFields(){
        if(empty($this->ad['link']) || empty($this->ad['title']))
            return false;

        return true;
    }

    /**
     * MAIN FUNCTION
     * @param $html
     * @return array|bool
     */
    public static function getData($html){
        $ob = new self($html);
        $ob->getAdSection();

        if(empty($ob->htmlAd))
            return false;

        $ob->getLink();
        $ob->getTitle();
        $ob->getBrand();

        if($ob->checkFields() == false)
            return false;

        return $ob->ad;
    }
}