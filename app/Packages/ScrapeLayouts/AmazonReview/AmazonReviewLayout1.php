<?php
namespace App\Packages\ScrapeLayouts\AmazonReview;

use Exception;
use Carbon\Carbon;

use App\Packages\simple_html_dom;
use App\Packages\ScrapeLayouts\AmazonReview\AmazonReviewAbstract;
use App\Packages\ScrapeLayouts\LayoutInterface;

/**
 * Class AmazonReviewLayout1
 * @package App\Packages\ScrapeLayouts\AmazonReview
 */
class AmazonReviewLayout1 extends AmazonReviewAbstract implements LayoutInterface
{
    public function __construct($html){
        parent::__construct($html);

    }


    public function getNormalReviews(){

        try{
            if(!$this->html)
                throw new \Exception("no html string");

            $_reviews = $this->html->find(".reviews .review");
            if(!$_reviews){
                throw new \Exception("html page doesn't match");
            }

            $reviews = array();
            foreach($_reviews as $_review){
                $r =  $this->get_review($_review, 'normal');

                if(!empty($r) && $this->checkFields($r)){
                    $reviews[] = $r;
                }
            }

            $this->reviews = $reviews;
            return $this->reviews;
        }catch(\Exception $e){
            return array();
        }

    }


    public function getPositiveReview(){
        try{
            if(!$this->html)
                throw new \Exception("no html string");

            $_review = $this->html->find(".positive-review" , 0);
            if(!$_review){
                throw new \Exception("html page doesn't match");
            }

            $positive_review =  $this->get_review($_review, 'positive');

            if($this->checkFields($positive_review))
                $this->positive_review = $positive_review;

            return $this->positive_review;

        }catch(\Exception $e){
            return array();
        }
    }


    public function getCriticalReview(){
        try{
            if(!$this->html)
                throw new \Exception("no html string");

            $_review = $this->html->find(".critical-review" , 0);
            if(!$_review){
                throw new \Exception("html page doesn't match");
            }

            $critical_review =  $this->get_review($_review, 'critical');

            if($this->checkFields($critical_review))
                $this->critical_review = $critical_review;

            return $this->critical_review;

        }catch(\Exception $e){
            return array();
        }
    }


    public function get_review($_review, $type = 'positive'){
        $review = array();
        $review['type'] = $type;

        //id for normal reviews
        $review_id = $_review;

        $_review = simple_html_dom::str_get_html($_review);

        if(empty($_review))
            return array();

        //title
        $title = $_review->find(".review-title",0);
        $review['title'] = !empty($title->innertext)? $title->innertext : "";

        //helpful text
        $helpful_count = $_review->find(".review-votes",0);
        $helpful_count= !empty($helpful_count->innertext)? $helpful_count->innertext : "";
        $review['helpful_count'] = intval($helpful_count) > 0 ? intval($helpful_count) : "";

        //Reviewer name
        $reviewer = $_review->find(".review-byline",0);
        $reviewer = !empty($reviewer->plaintext)? $reviewer->plaintext : "";
        $review['reviewer'] = preg_replace('/By /', '', $reviewer);

        //Review date
        $date = $_review->find(".review-date",0);
        $date = !empty($date->innertext)? $date->innertext : "";
        if(!empty($date)){
            $date = preg_replace('/on /', '', $date);
            $date = (new Carbon($date))->timestamp;
        }
        $review['date'] = $date;

        //Body
        if($type == "normal"){
            $body = $_review->find(".review-data .review-text",0);
        }else {
            $body = $_review->find(".a-expander-content",0)->last_child()->first_child();
        }
        $review['body'] = !empty($body->plaintext)? $body->plaintext : "";


        //review id
        if($type !== "normal"){
            $review_id = $_review->find(".a-expander-container",0);
        }
        $review['review_id'] = !empty($review_id->getAttribute("id")) ? $review_id->getAttribute("id") : '' ;
        $review['review_id'] = str_replace('viewpoint-', '', $review['review_id']);

        //verified purchase
        $verified_purchase = $_review->find(".review-data",0);
        $review['verified_purchase'] = !empty($verified_purchase->plaintext) ? 1 : 0;

        //badges
        $badges = $_review->find(".c7y-badge-link");
        $review['badges'] = "";
        if(!empty($badges)){
            foreach($badges as $badge){
                $review['badges'] .= $badge->plaintext. ", ";
            }
        }

        //videos
        $videos = $_review->find(".video-block");
        $review['videos_count'] = 0;
        if(!empty($videos)){
            $review['videos_count'] = count($videos);
        }

        //images
        $images = $_review->find(".review-image-tile");
        $review['images_count'] = 0;
        if(!empty($images)){
            $review['images_count'] = count($images);
        }

        return $review;
    }

    /**
     * fields
     * @return bool
     */
    public function checkFields($review){
        if(empty($review['title']) || empty($review['review_id']) || empty($review['reviewer']))
            return false;

        return true;
    }

    /**
     * MAIN FUNCTION
     * @param $html
     * @return array|bool
     */
    public static function getData($html){
        $ob = new self($html);
        return $ob;
    }
}