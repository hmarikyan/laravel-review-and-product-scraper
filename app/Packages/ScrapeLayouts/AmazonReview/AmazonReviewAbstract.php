<?php

namespace App\Packages\ScrapeLayouts\AmazonReview;

/**
 * Class AmazonReviewAbstract
 * @package App\Packages\ScrapeLayouts\AmazonReview
 */
abstract class AmazonReviewAbstract
{
    protected $html;

    public $reviews = array();
    public $critical_review  = array();
    public $positive_review  = array();


    public function __construct($html){
        $this->html = $html;
    }

    abstract public function getNormalReviews();
    abstract public function getPositiveReview();
    abstract public function getCriticalReview();

    abstract public function checkFields($review);
}