<?php

namespace App\Packages\ScrapeLayouts;

/**
 * Interface LayoutInterface
 * @package App\Packages\ScrapeLayouts
 */
interface LayoutInterface
{
    public static function getData($html);
}