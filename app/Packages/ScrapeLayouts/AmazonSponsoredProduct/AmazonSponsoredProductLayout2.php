<?php
namespace App\Packages\ScrapeLayouts\AmazonSponsoredProduct;

use Exception;

use App\Packages\simple_html_dom;
use App\Packages\ScrapeLayouts\AmazonSponsoredProduct\AmazonSponsoredProductAbstract;
use App\Packages\ScrapeLayouts\LayoutInterface;

/**
 * Class AmazonSponsoredProductLayout2
 * @package App\Packages\ScrapeLayouts\AmazonSponsoredProduct
 */
class AmazonSponsoredProductLayout2 extends AmazonSponsoredProductAbstract implements LayoutInterface
{
    public function __construct($html){
        parent::__construct($html);
    }

    /**
     * PRODUCTS SECTION in HTML
     */
    public function getProductsSection(){

        if(!$this->htmlProducts = $this->html->find("#desktop-rhs-carousels_click_within_right .pa-ad-details")){
            $this->htmlProducts = $this->html->find("#sponsored-products-rhs-widget .sponsored-products-product-section");
        }
    }

    /**
     * ITERATION
     */
    public function getProducts(){
        $this->products = array();

        foreach($this->htmlProducts as $pr){
            $this->product = array();

            $this->getType();
            $this->getAsin($pr);
            $this->getTitle($pr);
            $this->getCurrentPrice($pr);
            $this->getOriginalPrice($pr);
            $this->getIsPrime($pr);


            if($this->checkFields())
                $this->products[] = $this->product;
            //else continue;
        }
    }



    public function getAsin($pr){
        $asin = $pr->find("a", -1);
        $asin = !empty($asin) ? $asin->getAttribute("href") : "";
        preg_match("/product-reviews\/([\d\w]+)\//is", $asin, $output_array);
        $this->product['asin'] = !empty($output_array) ? $output_array[1]: "";
    }


    public function getTitle($pr){
        $title = $pr->find(".pa-str-truncate", 0);
        $this->product['title'] = !empty($title) ? $title->plaintext : '';
    }


    public function getCurrentPrice($pr){
        $current_price = $pr->find(".a-color-price", 0);
        $current_price = !empty($current_price) ? $current_price->plaintext : '';
        $this->product['current_price'] = str_replace("$","", $current_price);
    }

    public function getOriginalPrice($pr){
        $original_price = $pr->find(".a-text-strike", 0);
        $original_price = !empty($original_price) ? $original_price->plaintext : '';
        $this->product['original_price'] = str_replace("$","", $original_price);
    }

    public function getIsPrime($pr){
        $is_prime = $pr->find(".a-icon-prime",0);
        $this->product['is_prime'] = !empty($is_prime);
    }

    public function getReviewAvg($pr){
        $this->product['review_avg'] = "0";

        $review_avg = $pr->find(".a-icon-star", 0);
        $review_avg = !empty($review_avg) ? $review_avg->plaintext : '' ;
        if(!empty($review_avg)){
            $review_avg = trim($review_avg);
            $review_avg = explode(' ', $review_avg);
            $this->product['review_avg'] = $review_avg[0];
        }
    }

    public function getReviewCount($pr){
        $review_count = $pr->find("a", -1);
        $this->product['review_count'] = !empty($review_count) ? $review_count->plaintext : '' ;
    }


    /**
     * product fields
     * @return bool
     */
    public function checkFields(){
        if(empty($this->product['asin']) || empty($this->product['title']))
            return false;

        return true;
    }

    /**
     * MAIN FUNCTION
     * @param $html
     * @return array|bool
     */
    public static function getData($html){
        $ob = new self($html);
        $ob->getProductsSection();

        if(empty($ob->htmlProducts))
            return false;

        $ob->getProducts();

        if(empty($ob->products))
            return false;

        return $ob->products;
    }
}