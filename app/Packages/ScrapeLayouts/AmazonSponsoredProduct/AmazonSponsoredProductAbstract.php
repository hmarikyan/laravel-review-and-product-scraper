<?php

namespace App\Packages\ScrapeLayouts\AmazonSponsoredProduct;

/**
 * Class AmazonSponsoredProductAbstract
 * @package App\Packages\ScrapeLayouts\AmazonSponsoredProduct
 */
abstract class AmazonSponsoredProductAbstract
{
    protected $html;
    protected $htmlProducts;

    protected $products = array();
    protected $product = array();

    public function __construct($html){
        $this->html = $html;
    }

    abstract public function getProductsSection();
    abstract public function getProducts();

    abstract public function getAsin($pr);
    abstract public function getTitle($pr);
    abstract public function getCurrentPrice($pr);
    abstract public function getOriginalPrice($pr);
    abstract public function getIsPrime($pr);
    abstract public function getReviewAvg($pr);
    abstract public function getReviewCount($pr);

    public function getType(){
        $this->product['type'] = "sponsored";
    }

    abstract public function checkFields();
}