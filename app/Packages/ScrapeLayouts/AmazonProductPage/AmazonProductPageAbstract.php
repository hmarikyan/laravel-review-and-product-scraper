<?php

namespace App\Packages\ScrapeLayouts\AmazonProductPage;

/**
 * Class AmazonProductPageAbstract
 * @package App\Packages\ScrapeLayouts\AmazonProductPage
 */
abstract class AmazonProductPageAbstract
{
    protected $html;

    protected $product_page = array();

    public function __construct($html){
        $this->html = $html;
    }

    abstract public function getResultTotal();
    abstract public function getResultShown();
    abstract public function getSuggestedCategory();
    abstract public function getSuggestedKeywords();
    abstract public function getSeedKeywords();

    abstract public function checkFields();
}