<?php
namespace App\Packages\ScrapeLayouts\AmazonProductPage;

use Exception;

use App\Packages\simple_html_dom;
use App\Packages\ScrapeLayouts\AmazonProductPage\AmazonProductPageAbstract;
use App\Packages\ScrapeLayouts\LayoutInterface;

/**
 * Class AmazonProductPageLayout1
 * @package App\Packages\ScrapeLayouts\AmazonProductPage
 */
class AmazonProductPageLayout1 extends AmazonProductPageAbstract implements LayoutInterface
{
    public function __construct($html){
        parent::__construct($html);
    }


    public function getResultTotal(){
        $results = $this->html->find("#s-result-count", 0);
        preg_match("/of\b(.*)\bresults/", $results->plaintext, $result_total);
        $result_total = !empty($result_total[1])? $result_total[1] : '';
        $this->product_page['result_total'] = str_replace(",",'', $result_total);
    }

    public function getResultShown(){
        $results = $this->html->find("#s-result-count", 0);
        preg_match("/^[\d]*-[\d]*/", $results->plaintext, $result_shown);
        $this->product_page['result_shown'] = !empty($result_shown[0])? $result_shown[0] : '';
    }

    public function getSuggestedCategory(){
        $suggested_category = $this->html->find("#autoscoping-backlink  span.a-text-bold", 0);
        $this->product_page['suggested_category'] = !empty($suggested_category->innertext)? $suggested_category->innertext : '';
    }

    public function getSuggestedKeywords(){

        $this->product_page['suggested_keywords'] = "";

        preg_match("/Related.*?<\/div>/s", $this->html->innertext, $output_array);

        if (isset($output_array[0])) {
            $mainBlock = $output_array[0];
            preg_match_all("/(?:<span.*?<a.*?>)(.*?)(?:<\/a>.*?<\/span>)/s", $mainBlock, $output_array);

            if (isset($output_array[1])) {
                $this->suggestedKeywordArray = $output_array[1];
                $this->product_page['suggested_keywords'] = implode(', ', $output_array[1]);
            }
        }
    }

    public function getSeedKeywords(){
        $this->product_page['seed_keywords'] = "";
    }


    /**
     * check product page fields
     * @return bool
     */
    public function checkFields(){
        if(empty($this->product_page['result_shown']) || empty($this->product_page['result_total']))
            return false;

        return true;
    }

    /**
     * MAIN FUNCTION
     * @param $html
     * @return array|bool
     */
    public static function getData($html){
        $ob = new self($html);
        $ob->getResultTotal();
        $ob->getResultShown();
        $ob->getSuggestedCategory();
        $ob->getSuggestedKeywords();
        $ob->getSeedKeywords();

        if($ob->checkFields() == false)
            return false;

        return $ob->product_page;
    }
}