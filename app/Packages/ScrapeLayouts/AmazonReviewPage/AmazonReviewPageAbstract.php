<?php

namespace App\Packages\ScrapeLayouts\AmazonReviewPage;

/**
 * Class AmazonReviewPageAbstract
 * @package App\Packages\ScrapeLayouts\AmazonReviewPage
 */
abstract class AmazonReviewPageAbstract
{
    protected $html;

    protected $review_page = array();

    public function __construct($html){
        $this->html = $html;
    }

    abstract public function getTitle();
    abstract public function getAvgRating();
    abstract public function getTotalReviews();
    abstract public function NextPageAvailable();

    abstract public function checkFields();
}