<?php
namespace App\Packages\ScrapeLayouts\AmazonReviewPage;

use Exception;

use App\Packages\simple_html_dom;
use App\Packages\ScrapeLayouts\AmazonReviewPage\AmazonReviewPageAbstract;
use App\Packages\ScrapeLayouts\LayoutInterface;


class AmazonReviewPageLayout1 extends AmazonReviewPageAbstract implements LayoutInterface
{
    public function __construct($html){
        parent::__construct($html);
    }


    public function getTitle(){

        try{
            $title_div = $this->html->find("div.product-title",0 );
            $title_div = simple_html_dom::str_get_html($title_div);
            $title = $title_div->find("a.a-link-normal",0)->innertext;

            $this->review_page['title'] = $title;

        }catch(\Exception $e){
            $this->review_page['title'] = null;
        }
    }

    public function getAvgRating(){
        try{
            $average_rating = $this->html->find(".arp-rating-out-of-text", 0);
            $average_rating = !empty($average_rating->innertext) ? $average_rating->innertext : "";
            $this->review_page['average_rating'] = floatval($average_rating);

        }catch(\Exception $e){
            $this->review_page['average_rating'] = null;
        }
    }

    public function getTotalReviews(){
        try{
            $total_reviews = $this->html->find(".totalReviewCount", 0);
            $total_reviews = !empty($total_reviews->innertext) ? $total_reviews->innertext : "" ;
            $this->review_page['total_reviews'] = str_replace( ',', '', $total_reviews );
        }catch(\Exception $e){
            $this->review_page['total_reviews'] = null;
        }
    }

    public function NextPageAvailable(){

        $paging = $this->html->find(".a-pagination",0 );

        $paging = simple_html_dom::str_get_html($paging);
        $this->review_page['next_page_url'] = false;

        if(!empty($paging)){
            $last_link = $paging->find("li.a-last a", 0 );

            if(!empty($last_link)){
                $this->review_page['next_page_url'] = $last_link->href;
                return true;
            }else{
                return false;
            }
        }

        return false;
    }


    /**
     * check product page fields
     * @return bool
     */
    public function checkFields(){
        if(empty($this->review_page['title']) || empty($this->review_page['average_rating']))
            return false;

        return true;
    }

    /**
     * MAIN FUNCTION
     * @param $html
     * @return array|bool
     */
    public static function getData($html){
        if(!$html)
            return false;

        $ob = new self($html);

        $ob->getTitle();
        $ob->getAvgRating();
        $ob->getTotalReviews();
        $ob->NextPageAvailable();

        if($ob->checkFields() == false)
            return false;

        return $ob->review_page;
    }
}