<?php

namespace App\Packages\Utils;

use App\Models\AmazonProducts;
use App\Models\AmazonAsinList;

class ASINManager {

    public static function groupAndFill(){

        $asins =  AmazonProducts::select("asin")->groupBy("asin")->get();

        foreach($asins as $asin){
            $model = new AmazonAsinList();
            $model->asin = $asin->asin;
            $model->save();
        }
    }
}