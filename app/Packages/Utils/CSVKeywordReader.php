<?php

namespace App\Packages\Utils;

class CSVKeywordReader {

    public static function getKeywords($file){

        $data = str_getcsv($file, "\n");

        return $data;
    }

    public static function getKeywordsCSV(){
        $filename = storage_path("keywords.csv");
        $file = file_get_contents($filename);

        return self::getKeywords($file);
    }
}