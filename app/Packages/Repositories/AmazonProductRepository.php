<?php

namespace App\Packages\Repositories;

use Exception;

use App\Packages\Repositories\RepositoryInterface;
use App\Packages\Repositories\AbstractRepo;

use App\Models\AmazonProducts;
use App\Models\AmazonAds;
use App\Models\AmazonProductPages;


/**
 * Class AmazonReviewRepository
 */
class AmazonProductRepository extends AbstractRepo implements RepositoryInterface
{
    /**
     * @var
     */
    protected $error = null;

    /**
     * @param array $data
     */
    public function create(Array $data){
        try{
            if(!empty($data['product_page'])){
                $_pp = new AmazonProductPages();
                $_pp = self::load_model_data($_pp,$data['product_page']);
                $_pp->save();

                if(!empty($data['ad'])){
                    $_ad = new AmazonAds();
                    $_ad = self::load_model_data($_ad,$data['ad']);
                    $_ad->amazon_product_page_id =  $_pp->id;
                    $_ad->save();
                }

                if(!empty($data['products'])){
                    foreach($data['products'] as $product){
                        $_pr = new AmazonProducts();
                        $_pr = self::load_model_data($_pr, $product);
                        $_pr->amazon_product_page_id = $_pp->id;
                        $_pr->save();
                    }
                }

            }else
                throw new Exception("No product page details detected");

            return true;

        } catch(Exception $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}