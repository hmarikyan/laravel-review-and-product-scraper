<?php

namespace App\Packages\Repositories;

abstract class AbstractRepo{

    /**
     * INSTANTINATE MODEL
     * @param $model
     * @param $data
     */
    public static function load_model_data($model, $data){
        foreach($data as $k=>$v){
            $model->$k = $v;
        }

        return $model;
    }

    /**
     * @return mixed
     */
    public function getError(){
        return !empty($this->error) ?  $this->error : null;
    }
}