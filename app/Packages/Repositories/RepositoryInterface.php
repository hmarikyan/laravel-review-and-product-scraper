<?php

namespace App\Packages\Repositories;

/**
 * Interface RepositoryInterface
 * @package App\Packages\Repositories
 */
interface RepositoryInterface {

    /**
     * @param array $data
     * @return mixed
     */
    public function create(Array $data);

}