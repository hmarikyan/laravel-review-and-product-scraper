<?php

namespace App\Packages\Repositories;

use Exception;

use App\Packages\Repositories\RepositoryInterface;
use App\Packages\Repositories\AbstractRepo;

use App\Models\AmazonReviewPages;
use App\Models\AmazonRatings;
use App\Models\AmazonReviews;


/**
 * Class AmazonReviewRepository
 */
class AmazonReviewRepository extends AbstractRepo implements RepositoryInterface
{
    /**
     * @var
     */
    protected $error = null;

    /**
     * @param array $data
     */
    public function create(Array $data){
        try{
            /*review_page*/
            if(!empty($data['review_page']) && empty($data['review_page']->id)) {
                $review_page = new AmazonReviewPages();
                $review_page = self::load_model_data($review_page, $data['review_page']);
                $review_page->save();

                $data['review_page'] = $review_page;
            } elseif(!empty($data['review_page']) && !empty($data['review_page']->id)){

            }else
                throw new Exception("No Review page");
            /*end review page*/

            /*RATING*/
            if(!empty($data['ratings'])){
                foreach($data['ratings'] as $_rating){
                    $rating = new AmazonRatings();
                    $rating = self::load_model_data($rating, $_rating);
                    $rating->amazon_page_id = $data['review_page']->id;
                    $rating->save();
                }
            }
            /*end RATING*/

            /*REVIEWS*/
            if(!empty($data['reviews'])){
                foreach($data['reviews'] as $_review){
                    $review = new AmazonReviews();
                    $review = self::load_model_data($review, $_review);
                    $review->amazon_page_id = $data['review_page']->id;
                    $review->save();
                }
            }/*end REVIEWS*/

            return $data['review_page'];

        }catch(Exception $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}