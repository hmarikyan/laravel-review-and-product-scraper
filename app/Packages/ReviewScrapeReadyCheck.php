<?php

namespace App\Packages;

use App\Models\AmazonAsinList;
use Carbon\Carbon;

/**
 * Class ReviewScrapeReadyCheck
 * @package App\Packages
 */
class ReviewScrapeReadyCheck
{
    const ALL = 1;
    const RECENT = 2;


    public static function check(AmazonAsinList $model){
        if(empty($model->last_full_review_run))
            return self::ALL;

        $last_full_review = Carbon::createFromTimestamp($model->last_full_review_run);
        $now = Carbon::now();

        if( $now->diff($last_full_review)->days >= 7 ){
            return self::ALL;
        } else {
            return self::RECENT;
        }
    }
}