<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Jobs\ReviewScrapeHandler;

use App\Models\AmazonReviews;

/**
 * Class ReviewsPageScrapeRecentJob
 * @package App\Jobs
 */
class ReviewsPageScrapeRecentJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var null
     */
    public $asin;

    /**
     * @var null
     *
     */
    public $url;

    /**
     * @var null
     */
    public $most_recent_review_id;

    /**
     * @var null
     */
    public $page_number;


    /**
     * Create a new job instance.
     *
     * $asin : amazon product identifier
     * $url : optional amazon review url
     *
     * @return void
     */
    public function __construct($asin, $url = null, $page_number = 1, $most_recent_review_id = null)
    {
        $this->asin = $asin;

        $this->url = $url;

        $this->most_recent_review_id = $most_recent_review_id;

        $this->page_number = $page_number;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //get most recent review
        $query = AmazonReviews::select('amazon_reviews.id', 'amazon_reviews.amazon_page_id', 'amazon_reviews.date')
            ->join('amazon_review_pages', 'amazon_reviews.amazon_page_id', '=', 'amazon_review_pages.id');

        if(!is_null($this->most_recent_review_id )){
            $query->where('amazon_reviews.id', '=' ,  $this->most_recent_review_id);
        }else{
            $query->where('amazon_review_pages.url', 'like' ,  '%'.$this->asin.'%');
            $query->orderBy("amazon_reviews.date", 'desc');
        }

        $most_recent_review = $query->first();


        (new ReviewScrapeHandler($this->asin, false))->handle($most_recent_review, $this->url, $this->page_number);
    }
}
