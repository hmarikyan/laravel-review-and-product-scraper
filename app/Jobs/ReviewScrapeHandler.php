<?php

namespace App\Jobs;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Traits\ScrapeTrait;

use App\Jobs\ReviewsPageScrapeAllJob;
use App\Jobs\ReviewsPageScrapeRecentJob;

use App\Packages\SuggestedSearchScrape\AmazonReviewScraper;
use App\Packages\Repositories\AmazonReviewRepository;

use App\Models\AmazonAsinList;
use App\Models\ProxyBlackList;


/**
 * Class ReviewScrapeHandler
 * @package App\Jobs
 */
class ReviewScrapeHandler
{
    use DispatchesJobs;
    use ScrapeTrait;


    public $asin;
    public $search_all = null;
    public $most_recent_review = null;

    public $web_page = null;
    public $url = null;

    /**
     * @var int
     */
    public $all_reviews_count = 0;

    /*
     * how many pages by pagination need to scrap
     */
    const PAGE_COUNT = 5;

    /**
     * @param null $asin
     * @param bool|true $search_all
     */
    public function __construct($asin = null, $search_all = true){
        $this->search_all = $search_all;
        $this->asin = $asin;
    }


    /**
     * @param null $most_recent_review
     *     model passed - in case seach_all = 1
     * @param null $url
     *     ready url to be scraped
     * @param page_number
     *      pagination
     */
    public function handle($most_recent_review = null, $paging_url = null, $page_number = 1){
        try{
            $this->info("");
            $this->info("Starting review scraping. ASIN: ". $this->asin);

            // if there is no any existing review for this $asin
            if(empty($most_recent_review) && $this->search_all != true){
                $this->search_all = true;
                $this->info(" Most recent review is Empty");
            }
            $this->most_recent_review = $most_recent_review;

            //build url if needed
            if(is_null($paging_url)){
                $url = $this->buildUrl($this->asin);
            }else{
                $url = $paging_url;
            }
            $this->url = $url;

            //get html data
            $this->web_page = $this->getSource($this->url);

            if(!$this->web_page){
                throw new \Exception("No html page");
            }
            elseif($this->detect_captcha($this->web_page)){
                ProxyBlackList::add($this->proxy);
                throw new \Exception("This is the captcha page");
            }

            $amazon_scrapper = new AmazonReviewScraper(null, $this->web_page);
            $amazon_scrapper->set_url($url);

            //get DATA
            $positive_review = $amazon_scrapper->get_positive_review();
            $critical_review = $amazon_scrapper->get_critical_review();
            $ratings = $amazon_scrapper->get_ratings();
            $review_page = $amazon_scrapper->get_review_page();

            if(empty($review_page)){
                $this->info(" Incorrect review page or url");
                return false;
            }else{
                $this->info(" Review page scrapped");
            }

            $this->info(" Scrapping reviews on page, Page number:". $page_number );
            $normal_reviews = $amazon_scrapper->get_normal_reviews();

            //logic for recent recent reviews
            if($this->search_all == false && !empty($normal_reviews)){
                $nws = array();
                foreach($normal_reviews as $nw){
                    if($nw['date'] > $this->most_recent_review->date){
                        $nws[] = $nw;
                    }
                }

                $amazon_scrapper->reviews = $nws;
            }
            $this->all_reviews_count += count($amazon_scrapper->reviews);

            $amazon_scrapper->save();

            if($amazon_scrapper->next_page_available()){

                if(++$page_number > self::PAGE_COUNT){
                    $this->info(" Pagination limit is reached");
                    return false;
                }elseif($this->search_all == false && $this->all_reviews_count == 0){
                    $this->info(" No added recent reviews, no need to scrap the next page");
                    return false;
                }

                $url = $this->buildNextPageUrl($amazon_scrapper->next_page_url);

                //select next job class
                if($this->search_all == true){
                    $job_class = ReviewsPageScrapeAllJob::class;
                    $most_recent_review_id = null;
                }else{
                    $job_class = ReviewsPageScrapeRecentJob::class;
                    $most_recent_review_id = $this->most_recent_review->id;
                }

                //add job onto queue
                $job = (new $job_class($this->asin, $url, $page_number, $most_recent_review_id))->onQueue('amazon_scrape_reviews');
                $this->dispatch($job);

                $this->info("Added new Job for scrapping the next page.");
            }else{
                $this->info(" Next page is not available");
            }

            //update
            if($this->search_all == true && $asin_list = AmazonAsinList::where('asin', $this->asin)->first()){
                $asin_list->last_full_review_run = time();
                $asin_list->save();

                $this->info(" Updated asin_list -> last_full_review_run");
            }

            $this->info("Finished: Added ".$this->all_reviews_count." reviews to db.");

            $this->info("Sleeping 15 sec");
            sleep(15);

        } catch (\Exception $e){
            $this->info("Error! ". $e->getMessage());
        }

        return;
    }

    /**
     * @param $asin
     * @return string
     */
    public function buildUrl($asin){
        $url = "http://www.amazon.com/product-reviews/". $asin."/ref=cm_cr_arp_d_viewopt_srt?sortBy=recent";
        return $url;
    }

    /**
     * @param $next_page_url
     * @return string
     */
    public function buildNextPageUrl($next_page_url){
        $url = "http://www.amazon.com".$next_page_url;
        return $url;
    }
}