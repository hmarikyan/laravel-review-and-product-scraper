<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;


use App\Jobs\SearchResultScrapeHandler;

class SearchResultsPageScrapeJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $keyword;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($keyword = "")
    {
        $this->keyword = $keyword;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new SearchResultScrapeHandler($this->keyword))->handle();
    }
}
