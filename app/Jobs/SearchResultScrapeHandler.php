<?php

namespace App\Jobs;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Traits\ScrapeTrait;

use App\Packages\SuggestedSearchScrape\AmazonProductScraper;

use App\Models\ProxyBlackList;

/**
 * Class SearchResultScrapeHandler
 * @package App\Jobs
 */
class SearchResultScrapeHandler
{
    use DispatchesJobs;
    use ScrapeTrait;

    public $keyword;

    /**
     * @param $keyword
     */
    public function __construct($keyword){
        $this->keyword = $keyword;
    }

    /**
     * SCRAP DATA
     */
    public function handle(){
        try{
            $this->info("");
            $this->info("Starting search page scrapping. Keyword: ". $this->keyword);

            //build url
            $url = $this->buildUrl($this->keyword);

            //get html data
            $web_page = $this->getSource($url);

            if(!$web_page){
                throw new \Exception("No html page");
            }
            elseif($this->detect_captcha($web_page)){
                ProxyBlackList::add($this->proxy);
                throw new \Exception("This is the captcha page");
            }


            $amazon_scrapper = new AmazonProductScraper(null, $web_page);
            $amazon_scrapper->set_url($url);

            $amazon_scrapper->prepare();
            $amazon_scrapper->set_seed_keywords($this->keyword);
            $result = $amazon_scrapper->save();

            $this->info("Finished!");

            $this->info("Sleeping 15 sec");
            sleep(15);
        }catch (\Exception $e){
            $this->info("Error! ". $e->getMessage());
        }
    }


    /**
     * @param $keyword
     * @return string
     */
    public function buildUrl($keyword){
        $url = "http://www.amazon.com/s/ref=nb_sb_noss_1?field-keywords=".urlencode($keyword);
        return $url;
    }

}