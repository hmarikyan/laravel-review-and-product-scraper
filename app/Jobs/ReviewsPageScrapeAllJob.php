<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Jobs\ReviewScrapeHandler;

/**
 * Class ReviewsPageScrapeAllJob
 * @package App\Jobs
 */
class ReviewsPageScrapeAllJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var null
     */
    public $asin;

    /**
     * @var null
     *
     */
    public $url;

    /**
     * @var null
     */
    public $page_number;


    /**
     * Create a new job instance.
     *
     * $asin : amazon product identifier
     * $url : optional amazon review url
     * $page_number: which page on pagination
     *
     * @return void
     */
    public function __construct($asin, $url = null, $page_number = 1)
    {
        $this->asin = $asin;

        $this->url = $url;

        $this->page_number = $page_number;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new ReviewScrapeHandler($this->asin, true))->handle(null, $this->url, $this->page_number);
    }
}
