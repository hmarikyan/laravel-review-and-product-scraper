<?php

namespace App\Traits;

use App\Models\ProxyBlackList;

/**
 * Class ScrapeTrait
 * @package App\Traits
 */
trait ScrapeTrait{

    public $proxy;
    /**
     * @param $url
     * @return string
     */
    public function getSource($url){
        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
        ));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 7);
        curl_setopt($ch, CURLOPT_TIMEOUT, 7);

        //proxy
        do{
            $this->proxy = $pr = \Proxy::randomProxy('main_proxies');
        }
        while(!ProxyBlackList::is_available($pr));
        curl_setopt($ch, CURLOPT_PROXY, $pr);

        //user agent
        $this->user_agent = $ua = \Proxy::randomUserAgent('chrome');
        curl_setopt($ch, CURLOPT_USERAGENT, $ua);

        // Send the request & save response to $resp
        $resp = curl_exec($ch);
        curl_close($ch);

        // SET DEBUG DATA
        \Bugsnag::setBeforeNotifyFunction(function($error) use ($resp, $url){
            $error->setMetaData([
                'HTML' => [
                    'Source' => $resp,
                    'Url' => $url
                ]
            ]);
        });

        return $resp;
    }

    /**
     * @param string $text
     */
    public function info($text){
        echo $text."\r\n";
    }

    /**
     * @return bool
     */
    public function detect_captcha($html){
        if(strpos($html, "Type the characters you see in this image") !== false)
            return true;

        return false;
    }
}