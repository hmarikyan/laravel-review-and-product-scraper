<?php

namespace App\Http\Middleware;

use Closure;
use Exception;

class AuthApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            /*VALIDATE INPUT PARAMETER*/
            $request_token = $request->input('api_token');
            if(empty($request_token))
                throw new Exception("Missing API Token");

            /*VALIDATE ENV PARAMETER*/
            $env_token = env("API_TOKEN");
            if(empty($env_token))
                throw new Exception("Missing ENV Parameter - API Token");

            /*VALIDATE TOKEN*/
            if($request_token != $env_token)
                throw new Exception("API Token doesn't match!");

        }catch(Exception $e){
            $response = array(
                'status' => false,
                'error' => 'Unauthorized: '. $e->getMessage()
            );
            return response()->json($response, 401);
        }

        return $next($request);
    }
}
