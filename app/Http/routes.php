<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*---------- TEST ---------*/
Route::get('/scrap', "ScrapController@index");
Route::any('/scrap/do_reviews', "ScrapController@do_reviews");
Route::any('/scrap/do_products', "ScrapController@do_products");

Route::get('/scrap/queue_review', "ScrapController@queue_review");
Route::get('/scrap/queue_product/{k}', "ScrapController@queue_product");

Route::get('/scrap/proxy', "ScrapController@proxy");
Route::get('/scrap/proxy_black', "ScrapController@proxy_black");

Route::get('/scrap/bugsnag', "ScrapController@bugsnag");
/*end---------- TEST ---------*/


/*---------- API ROUTING --------------*/
Route::group(['middleware' => 'auth.api', 'prefix' => '/api', 'namespace' => 'Api'], function () {
    Route::post('/asin', 'CommonController@insert_asin');
    Route::post('/keyword', 'CommonController@insert_keyword');

    Route::post('/get_review_data', 'ReviewController@get_review_data');
    Route::post('/get_product_keyword_data', 'ProductController@get_product_keyword_data');
});
/*end---------- API ROUTING --------------*/
