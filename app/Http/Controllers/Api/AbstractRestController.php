<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


/**
 * Class CommonController
 * @package App\Http\Controllers
 */
abstract class AbstractRestController extends Controller
{
    protected $response;
    protected $status_code;

    const REVIEW_RETURN_COUNT = 20;

    public function __construct(){

        $this->response = array(
            'status' => true
        );

        $this->status_code = 200;
    }
}
