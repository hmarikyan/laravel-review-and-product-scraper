<?php

namespace App\Http\Controllers\Api;

use Exception;

use Illuminate\Http\Request;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\AmazonAsinList;
use App\Models\AmazonKeywordList;

/**
 * Class CommonController
 * @package App\Http\Controllers
 */
class CommonController extends AbstractRestController
{
    public function __construct(){
        parent::__construct();
    }

    /**
     * POST REQUEST
     * @return response
     */
    public function insert_asin(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'asin' => 'required|string|unique:amazon_asin_list|max:50',
                'marketplace_id' => 'string|max:30',
            ]);

            if ($validator->fails()) {

                throw new Exception(json_encode($validator->errors()->all()));
            }

            $model = new AmazonAsinList();
            $model->asin = $request->input('asin');
            $model->marketplace_id = $request->input('marketplace_id');

            if($model->save()){
                $this->response['model'] = $model;
            }

        } catch(\PDOException $e){
            $this->status_code = 500;
            $this->response = array(
                'status'=> false,
                'error' => $e->getMessage()
            );

        } catch(Exception $e){
            $this->status_code = 400;
            $this->response = array(
                'status'=> false,
                'error' => $e->getMessage()
            );
        }

        return response()->json($this->response, $this->status_code);
    }

    /**
     * POST REQUEST
     * @return response
     */
    public function insert_keyword(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'keyword' => 'required|string|unique:amazon_keyword_list|max:100',
                'marketplace_id' => 'string|max:30',
            ]);

            if ($validator->fails()) {
                throw new Exception(json_encode($validator->errors()->all()));
            }

            $model = new AmazonKeywordList();
            $model->keyword = $request->input('keyword');
            $model->marketplace_id = $request->input('marketplace_id');

            if($model->save()){
                $this->response['model'] = $model;
            }

        } catch(\PDOException $e){
            $this->status_code = 500;
            $this->response = array(
                'status'=> false,
                'error' => $e->getMessage()
            );

        } catch(Exception $e){
            $this->status_code = 400;
            $this->response = array(
                'status'=> false,
                'error' => $e->getMessage()
            );
        }

        return response()->json($this->response, $this->status_code);
    }

}
