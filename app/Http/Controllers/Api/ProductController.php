<?php

namespace App\Http\Controllers\Api;

use Exception;

use Illuminate\Http\Request;
use Validator;
use DB;

use App\Http\Requests;


/**
 * Class CommonController
 * @package App\Http\Controllers
 */
class ProductController extends AbstractRestController
{
    public function __construct(){
        parent::__construct();
    }

    /**
     * POST REQUEST
     * @return response
     */
    public function get_product_keyword_data(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'asins' => 'required|string',
                'data_types' => 'required|string',
                'past_days' => 'numeric',
            ]);

            if ($validator->fails()) {
                throw new Exception(json_encode($validator->errors()->all()));
            }

            $asins = explode(',', $request->input('asins'));
            $data_types = $request->input('data_types');

           $results = array();
           foreach($asins as $asin){
               try {
                   $result = array();

                   //DATA TYPE
                   if(strpos($data_types,'ppc')!== false){

                       $prods = DB::table('amazon_products as ap')
                           ->where('ap.asin', trim($asin))
                           ->where('ap.type', 'sponsored')
                           ->orderBy('ap.id', 'asc')
                           ->leftJoin('amazon_product_pages as app', 'app.id', '=', 'ap.amazon_product_page_id')
                           ->select('ap.asin', 'ap.title', 'ap.brand', 'app.id as page_id', 'app.seed_keywords')
                           ->get();

                       $ppc = array();
                       foreach($prods as $prod){
                           $ppc[] = $prod->seed_keywords;
                       }

                       $result['ppc'] = $ppc;
                   }

                   //DATA TYPE
                   if(strpos($data_types,'normal')!== false){

                       $prods = DB::table('amazon_products as ap')
                           ->where('ap.asin', trim($asin))
                           ->where('ap.type', 'normal')
                           ->orderBy('ap.id', 'asc')
                           ->leftJoin('amazon_product_pages as app', 'app.id', '=', 'ap.amazon_product_page_id')
                           ->select('ap.asin', 'ap.title', 'ap.brand', 'app.id as page_id', 'app.seed_keywords')
                           ->get();

                       $normal = array();
                       foreach($prods as $prod){
                           $normal[] = $prod->seed_keywords;
                       }

                       $result['normal'] = $normal;
                   }

                   if(empty($result['normal']) && empty($result['normal'])){
                       throw new Exception("No such data for asin:". $asin);
                   }

               }catch(Exception $e){
                   $result = array('error' => $e->getMessage());
               }

               $results[$asin] = $result;
           }

            $this->response['results'] = $results;

        } catch(\PDOException $e){
            $this->status_code = 500;
            $this->response = array(
                'status'=> false,
                'error' => $e->getMessage()
            );

        } catch(Exception $e){
            $this->status_code = 400;
            $this->response = array(
                'status'=> false,
                'error' => $e->getMessage()
            );
        }

        return response()->json($this->response, $this->status_code);
    }
}
