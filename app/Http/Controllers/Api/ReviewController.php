<?php

namespace App\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;
use Validator;
use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;


/**
 * Class CommonController
 * @package App\Http\Controllers
 */
class ReviewController extends AbstractRestController
{
    public function __construct(){
        parent::__construct();
    }

    /**
     * POST REQUEST
     * @return response
     */
    public function get_review_data(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'asins' => 'required|string',
                'data_types' => 'required|string',
            ]);

            if ($validator->fails()) {
                throw new Exception(json_encode($validator->errors()->all()));
            }

            $asins = explode(',', $request->input('asins'));
            $data_types = $request->input('data_types');

           $results = array();
           foreach($asins as $asin){
              try {
                  $result = array();
                  $overview = DB::table('amazon_review_pages as arp')
                      ->where('arp.url', 'like' ,  '%'.trim($asin).'%')
                      ->select('arp.id', 'arp.url', 'arp.title', 'arp.average_rating' , 'arp.total_reviews')
                      ->orderBy('arp.id', 'desc')
                      ->first();

                  if(empty($overview)){
                      throw new Exception("No such data for asin:". $asin);
                  }
                  //DATA TYPE
                  if(strpos($data_types,'overview') !== false){
                      $result['overview'] = $overview;
                  }

                  //DATA TYPE
                  if(strpos($data_types,'breakdown')!== false){

                      $ratings = DB::table('amazon_ratings as ar')
                          ->where('ar.amazon_page_id', $overview->id)
                          ->whereIn('ar.star', [1,2,3,4,5])
                          ->select('ar.id', 'ar.star', 'ar.percentage')
                          ->orderBy('ar.id', 'desc')
                          ->groupBy('ar.star')
                          ->get();

                      $precentage = array();
                      $count = array();
                      foreach($ratings as $rt){
                          $precentage['stars_'.$rt->star] = $rt->percentage;
                          $count['stars_'.$rt->star] = round(($rt->percentage / 100) * $overview->total_reviews);
                      }

                      $result['breakdown'] = array(
                          'percentage' => $precentage,
                          'count' => $count
                      );
                  }

                  //DATA TYPE
                  if(strpos($data_types,'top')!== false){
                      $top_revs  = DB::table('amazon_reviews as ar')
                          ->where('ar.amazon_page_id', $overview->id)
                          ->whereIn('type', ['critical', 'positive'])
                          ->orderBy('ar.id', 'desc')
                          ->groupBy('ar.type')
                          ->get();

                      $top = array();
                      foreach($top_revs as $top_rev){
                          $top[$top_rev->type] = $top_rev;
                      };
                      $result['top'] =  $top;
                  }

                  //DATA TYPE
                  if(strpos($data_types,'reviews')!== false){
                      preg_match ( "/reviews:(\d*)/" , $data_types , $matches);
                      $start = 0;
                      if(!empty($matches[1]))
                          $start = $matches[1];

                      $revs  = DB::table('amazon_reviews as ar')
                          ->where('ar.amazon_page_id', $overview->id)
                          ->where('type', 'normal')
                          ->orderBy('ar.date', 'desc')
                          ->take(self::REVIEW_RETURN_COUNT)
                          ->skip($start)
                          ->get();

                      $result['reviews'] =  $revs;
                  }
              }catch(Exception $e){
                  $result = array('error' => $e->getMessage());
              }

               $results[$asin] = $result;
           }

            $this->response['results'] = $results;

        } catch(\PDOException $e){
            $this->status_code = 500;
            $this->response = array(
                'status'=> false,
                'error' => $e->getMessage()
            );

        } catch(Exception $e){
            $this->status_code = 400;
            $this->response = array(
                'status'=> false,
                'error' => $e->getMessage()
            );
        }

        return response()->json($this->response, $this->status_code);
    }
}
