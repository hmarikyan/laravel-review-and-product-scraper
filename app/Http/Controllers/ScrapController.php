<?php

namespace App\Http\Controllers;

use App\Models\AmazonReviewPages;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Packages\SuggestedSearchScrape\AmazonReviewScraper;
use App\Packages\SuggestedSearchScrape\AmazonProductScraper;

use App\Jobs\ReviewsPageScrapeAllJob;
use App\Jobs\ReviewsPageScrapeRecentJob;
use App\Jobs\SearchResultsPageScrapeJob;

use App\Models\ProxyBlackList;


class ScrapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \View::make('amazon_scrapper');
    }

    public function do_reviews()
    {
        //$url = \Input::get('url');
        //$url = "http://www.amazon.com/Alpha-Grillers-Garlic-Stainless-Silicone/product-reviews/B00I937QEI/ref=cm_cr_arp_d_viewopt_srt?ie=UTF8&showViewpoints=1&sortBy=recent&pageNumber=1";
        //$url = "http://www.amazon.com/Alpha-Grillers-Garlic-Stainless-Silicone/product-reviews/B00I937QEI/ref=cm_cr_getr_d_paging_btm_109?ie=UTF8&showViewpoints=1&sortBy=recent&pageNumber=109";
        $url = "http://www.amazon.com/product-reviews/B000GG0BM0/ref=cm_cr_arp_d_viewopt_srt?sortBy=recent";


        $web_page = file_get_contents($url);

        $amazon_scrapper = new AmazonReviewScraper(null, $web_page);
        $amazon_scrapper->set_url($url);

        $result = $amazon_scrapper->prepare()->save();

        $data = array();

        if($result == false){
            $data['error'] = $amazon_scrapper->error;
        }else{
            $data['result'] = $result;
        }

        return \View::make('amazon_review_data',$data);
    }

    public function do_products(){

        $url = \Input::get('url');
        //$url = "http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=garlic+press&rh=i%3Aaps%2Ck%3Agarlic+press";
        //$url = "http://www.amazon.com/gp/search/ref=sr_kk_3?rh=i%3Aaps%2Ck%3Agarlic+peeler&keywords=garlic+peeler&ie=UTF8&qid=1457052617";
        //$url = "http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=garcinia&rh=i%3Aaps%2Ck%3Agarcinia";
        //$url = "http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=watch&rh=i%3Aaps%2Ck%3Awatch";
        //$url = "http://www.amazon.com/s/ref=nb_sb_noss_1?url=search-alias%3Daps&field-keywords=shoes&rh=i%3Aaps%2Ck%3Ashoes";
        //$url = "http://www.amazon.com/s/ref=nb_sb_noss_1?url=search-alias%3Daps&field-keywords=fish+oil";
        //$url = "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=+amazing+garlic";

        $web_page = file_get_contents($url);

        $amazon_scrapper = new AmazonProductScraper(null, $web_page);
        $amazon_scrapper->set_url($url);

        $data = array();

        $amazon_scrapper->prepare();
        $amazon_scrapper->set_seed_keywords("garlic");
//
//        print_r($amazon_scrapper->get_page());
//        print_r($amazon_scrapper->get_products());
//        print_r($amazon_scrapper->get_sponsored_products());
//        print_r($amazon_scrapper->get_ad());

        $result = $amazon_scrapper ->save();

        if($result == false){
            $data['error'] = $amazon_scrapper->error;
        }else{
            $data['result'] = $result;
        }

        return \View::make('amazon_product_data',$data);
    }


    /*QUEUES*/
    public function queue_review()
    {
        $job = (new ReviewsPageScrapeRecentJob("B0094KOSO6"))->onQueue('amazon_scrape_reviews');
        $this->dispatch($job);

        $job = (new ReviewsPageScrapeRecentJob("B002PWYVOC"))->onQueue('amazon_scrape_reviews');
        $this->dispatch($job);
    }

    public function queue_product($k = "garlic press")
    {
        $job = (new SearchResultsPageScrapeJob($k))->onQueue('amazon_search_results_scrape');
        $this->dispatch($job);
    }


    public function proxy(){
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=garlic+press&rh=i%3Aaps%2Ck%3Agarlic+press',
        ));

        $pr = \Proxy::randomProxy('main_proxies');
        curl_setopt($curl, CURLOPT_PROXY, $pr);

        $ua = \Proxy::randomUserAgent('chrome');
        curl_setopt($curl, CURLOPT_USERAGENT, $ua);

        // Send the request & save response to $resp
        $resp = curl_exec($curl);

        // Close request to clear up some resources
        curl_close($curl);

        echo $resp;
    }

    public function proxy_black(){
        $proxy = '170.75.158.64:1191';

        //$model = ProxyBlackList::add($proxy);
        echo $is_available = ProxyBlackList::is_available($proxy);
    }


    public function bugsnag(){
        $source = "HELLO";


        \Bugsnag::setBeforeNotifyFunction(function($error) use ($source){
            $error->setMetaData([
                'HTML' => [
                    'Source' => $source
                ]
            ]);
        });

        \Bugsnag::notifyException(new \Exception('MY CUSTOM ERROR'));
       // throw new \Exception("MY CUSTOM ERROR");
    }

}
