<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AmazonKeywordList
 * @package App\Models
 */
class AmazonKeywordList extends Model
{
    /*
    * @var string
    */
    protected $table = 'amazon_keyword_list';
}
