<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmazonReviews extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'amazon_reviews';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page()
    {
        return $this->belongsTo('App\Models\AmazonReviewPages', 'amazon_page_id');
    }
}
