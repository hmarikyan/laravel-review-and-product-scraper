<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

/**
 * Class ProxyBlackList
 * @package App\Models
 */
class ProxyBlackList extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'proxy_black_list';

    const BLOCK_MINS = 15;

    /**
     * @param $proxy
     * @return ProxyBlackList
     */
    public static function add($proxy){
        if($model = self::where('proxy', $proxy)->first()){
            $model->touch();
        }else{
            $model = new ProxyBlackList();
            $model->proxy = $proxy;
            $model->save();
        }

        return $model;
    }

    /**
     * @param $proxy
     * @return bool
     */
    public static function is_available($proxy){
        $model = self::where('proxy', $proxy)->first();

        // not in blacklist
        if(!$model)
            return true;

        $dt = new Carbon($model->updated_at);
        $past_mins = $dt->diffInMinutes();

        //check time difference
        if($past_mins >= self::BLOCK_MINS){
            $model->delete();
            return true;
        }

        return false;
    }
}
