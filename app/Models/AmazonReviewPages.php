<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmazonReviewPages extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'amazon_review_pages';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany('App\Models\AmazonRatings', 'amazon_page_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany('App\Models\AmazonReviews', 'amazon_page_id');
    }
}
