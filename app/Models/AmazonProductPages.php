<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmazonProductPages extends Model
{

    /**
     * @var string
     */
    protected $table = 'amazon_product_pages';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ads()
    {
        return $this->hasMany('App\Models\AmazonAds', 'amazon_product_page_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\AmazonProducts', 'amazon_product_page_id');
    }
}
