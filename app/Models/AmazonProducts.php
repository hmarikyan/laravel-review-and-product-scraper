<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmazonProducts extends Model
{
    /**
     * @var string
     */
    protected $table = 'amazon_products';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_page()
    {
        return $this->belongsTo('App\Models\AmazonProductPage', 'amazon_product_page_id');
    }
}
