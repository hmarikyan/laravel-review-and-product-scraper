<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AmazonAsinList
 * @package App\Models
 */
class AmazonAsinList extends Model
{
    /*
    * @var string
    */
    protected $table = 'amazon_asin_list';
}
