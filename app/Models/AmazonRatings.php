<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmazonRatings extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'amazon_ratings';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\AmazonReviewPages', 'amazon_page_id');
    }
}
