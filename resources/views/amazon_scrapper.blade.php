<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'sans-serif';
                font-size: 16px;
            }

            .container {
                text-align: center;
                vertical-align: middle;
                margin-bottom: 20px;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
            .url_textbox {
                width: 1000px;
                /* height: 30px; */
                padding: 5px;
            }

        </style>
    </head>
    <body>
        <div class="container">
            Amazon Reviews
            <form action="/scrap/do_reviews" METHOD="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div>
                    <input type="text" class="url_textbox" value="http://www.amazon.com/Alpha-Grillers-Garlic-Stainless-Silicone/product-reviews/B00I937QEI/ref=cm_cr_arp_d_viewopt_srt?ie=UTF8&showViewpoints=1&sortBy=recent&pageNumber=1" placeholder="http://" name="url" />
                </div>
                <div>
                    <button type="submit"> SCRAP DATA</button>
                </div>
            </form>
        </div>

        <div class="container">
            Amazon Products
            <form action="/scrap/do_products" METHOD="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div>
                    <input type="text" class="url_textbox" value="http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=garlic+press&rh=i%3Aaps%2Ck%3Agarlic+press" placeholder="http://" name="url" />
                </div>
                <div>
                    <button type="submit"> SCRAP DATA</button>
                </div>
            </form>
        </div>
    </body>
</html>
