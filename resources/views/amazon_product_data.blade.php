<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-family: 'sans-serif';
                font-size: 16px;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
            .url_textbox{
                width: 300px;
            }

            table{
                font-size: 14px;
            }

        </style>
    </head>
    <body>
        <div class="container">

            @if (!empty($result['product_page']) > 0)
                <?php $p = (object)$result['product_page']; ?>

                <!-- -------PRODUCT PAGE------------->
                <div>
                    Product Page
                    <table border="1" cellpadding="1">
                        <thead>
                        <tr>
                            <td>Suggested Category</td>
                            <td>Suggested keywords</td>
                            <td>Seed keywords</td>
                            <td>Result shown</td>
                            <td>Result total</td>
                        </tr>
                        </thead>

                        <tr>
                            <td>{{ $p->suggested_category  }}</td>
                            <td>{{ $p->suggested_keywords  }}</td>
                            <td>{{ $p->seed_keywords  }}</td>
                            <td>{{ $p->result_shown  }}</td>
                            <td>{{ $p->result_total  }}</td>
                        </tr>
                    </table>
                </div>
                <!-- end -------PRODUCT PAGE------------->

                <!-- -------ADS ------------->
                @if (!empty($result['ad']) > 0)
                <?php $a = (object)$result['ad']; ?>
                    <div>
                        Ad
                        <table border="1" cellpadding="1">
                            <thead>
                            <tr>
                                <td>Title</td>
                                <td>Link</td>
                                <td>Brand</td>
                            </tr>
                            </thead>

                            <tr>
                                <td>{{ $a->title  }}</td>
                                <td>{{ $a->link  }}</td>
                                <td>{{ $a->brand  }}</td>
                            </tr>

                        </table>
                    </div>
               @endif
               <!-- end -------ADS ------------->

                <!-- -------PROCUCTS ------------->
                @if (!empty($result['products']) > 0)
                    <div>
                        Products
                        <table border="1" cellpadding="1">
                            <thead>
                            <tr>
                                <td>type</td>
                                <td>title</td>
                                <td>brand</td>
                                <td>asin</td>
                                <td>is_sponsored</td>
                                <td>is_prime</td>
                                <td>rank</td>
                                <td>current_price</td>
                                <td>original_price</td>
                                <td>review_avg</td>
                                <td>review_count</td>
                                <td>best_seller</td>
                                <td>promotions</td>
                                <td>promotions_details</td>
                                <td>offers</td>
                                <td>variations</td>
                            </tr>
                            </thead>

                            @foreach ($result['products'] as $p)
                                <?php $p = (object)$p; ?>
                                <tr>
                                    <td>{{ $p->type  }}</td>
                                    <td>{{ $p->title  }}</td>
                                    <td>{{ @$p->brand  }}</td>
                                    <td>{{ @$p->asin  }}</td>
                                    <td>{{ @$p->is_sponsored  }}</td>
                                    <td>{{ @$p->is_prime  }}</td>
                                    <td>{{ @$p->rank  }}</td>
                                    <td>{{ @$p->current_price  }}</td>
                                    <td>{{ @$p->original_price  }}</td>
                                    <td>{{ @$p->review_avg  }}</td>
                                    <td>{{ @$p->review_count  }}</td>
                                    <td>{{ @$p->best_seller  }}</td>
                                    <td>{{ @$p->promotions  }}</td>
                                    <td>{{ @$p->promotions_details  }}</td>
                                    <td>{{ @$p->offers  }}</td>
                                    <td>{{ @$p->variations  }}</td>
                                </tr>
                            @endforeach

                        </table>
                    </div>
                @endif
                <!-- end -------PROCUCTS ------------->

            @else
                {{ $error }}
            @endif

        </div>
    </body>
</html>
