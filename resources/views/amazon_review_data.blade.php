<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-family: 'sans-serif';
                font-size: 16px;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
            .url_textbox{
                width: 300px;
            }

            table{
                font-size: 14px;
            }

        </style>
    </head>
    <body>
        <div class="container">


            @if (!empty($result['review_page']) > 0)
                <?php $p = (object)$result['review_page']; ?>

                <!-- -------PRODUCT ------------->
                <div>
                    Review Page
                    <table border="1" cellpadding="1">
                        <thead>
                        <tr>
                            <td>Title</td>
                            <td>URL</td>
                            <td>Total reviews</td>
                            <td>Averagerating</td>
                            <td>Next page available</td>
                        </tr>
                        </thead>

                        <tr>
                            <td>{{ $p->title  }}</td>
                            <td>{{ $p->url  }}</td>
                            <td>{{ $p->total_reviews  }}</td>
                            <td>{{ $p->average_rating  }}</td>
                            <td>{{ $p->is_next_page ? '1' : '0' }}</td>
                        </tr>
                    </table>
                </div>
                <!-- end -------PRODUCT ------------->

                <!-- -------RATINGS ------------->
                @if (!empty($result['ratings']) > 0)
                    <div>
                        Ratings
                        <table border="1" cellpadding="1">
                            <thead>
                            <tr>
                                <td>Star</td>
                                <td>Percentage</td>
                            </tr>
                            </thead>

                            @foreach ($result['ratings'] as $r)
                                <?php $r = (object)$r; ?>
                                <tr>
                                    <td>{{ $r->star  }}</td>
                                    <td>{{ $r->percentage  }}</td>
                                </tr>
                            @endforeach

                        </table>
                    </div>
               @endif
               <!-- end -------RATINGS ------------->

                <!-- -------REVIEWS ------------->
                @if (!empty($result['reviews']) > 0)
                    <div>
                        Reviews
                        <table border="1" cellpadding="1">
                            <thead>
                            <tr>
                                <td>type</td>
                                <td>title</td>
                                <td>helpful_count</td>
                                <td>reviewer</td>
                                <td>date</td>
                                <td>body</td>
                                <td>review_id</td>
                                <td>verified_purchase</td>
                                <td>badges</td>
                                <td>videos_count</td>
                                <td>images_count</td>
                            </tr>
                            </thead>

                            @foreach ($result['reviews'] as $r)
                                <?php $r = (object)$r; ?>
                                <tr>
                                    <td>{{ $r->type  }}</td>
                                    <td>{{ $r->title  }}</td>
                                    <td>{{ $r->helpful_count  }}</td>
                                    <td>{{ $r->reviewer  }}</td>
                                    <td>{{ $r->date  }}</td>
                                    <td>{{ $r->body  }}</td>
                                    <td>{{ $r->review_id  }}</td>
                                    <td>{{ $r->verified_purchase  }}</td>
                                    <td>{{ $r->badges  }}</td>
                                    <td>{{ $r->videos_count  }}</td>
                                    <td>{{ $r->images_count  }}</td>
                                </tr>
                            @endforeach

                        </table>
                    </div>
                @endif
                <!-- end -------REVIEWS ------------->

            @else
                {{ $error }}
            @endif

        </div>
    </body>
</html>
