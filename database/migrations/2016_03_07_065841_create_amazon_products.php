<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmazonProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_products', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('amazon_product_page_id')->comment = 'Refers to amazon_product_pages table';

            $table->string('asin', 100)->comment = 'ASIN code';
            $table->string('rank', 50)->comment = "Result rank";
            $table->integer('is_sponsored')->comment = 'Sponsored or not';
            $table->integer('is_prime')->comment = 'Prime eligible';

            $table->string('title', 150)->comment = 'Product title';
            $table->string('brand', 100)->comment = 'Product brand';

            $table->double('current_price');
            $table->double('original_price');

            $table->double('review_avg')->comment = "Reviews cca value";
            $table->integer('review_count')->comment = "Reviews quantity";

            $table->text('best_seller')->comment = "Best seller badges";
            $table->integer('promotions')->default(0)->comment = "Promotions count";
            $table->text('promotions_details')->comment = "Promotions content";

            $table->text('offers')->comment = "Other offers";
            $table->string('variations', 250)->default(0)->comment = "variations : See Color Options, See more choices ";

            $table->string('type')->default('normal')->comment = "Product type sponsored:normal";

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('amazon_products');
    }
}
