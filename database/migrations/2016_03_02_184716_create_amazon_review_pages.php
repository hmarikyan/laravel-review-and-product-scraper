<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmazonReviewPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_review_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 250)->comment = 'Product name';
            $table->text('url', 250)->comment = 'Product url';
            $table->integer('total_reviews')->comment = 'Total eview count';
            $table->double('average_rating')->comment = 'Average review rating';
            $table->integer('is_next_page')->comment = 'Next page of reviews available';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('amazon_review_pages');
    }
}
