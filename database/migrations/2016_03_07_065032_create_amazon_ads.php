<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmazonAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amazon_product_page_id')->comment = 'Refers to amazon_product_pages table';

            $table->string('title', 200)->comment = 'Ad title';
            $table->text('link')->default('');
            $table->string('brand',100)->comment = 'Advertizer';

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('amazon_ads');
    }
}
