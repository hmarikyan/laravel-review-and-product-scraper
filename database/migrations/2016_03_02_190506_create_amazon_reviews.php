<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmazonReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amazon_page_id')->comment = 'Refers to amazon_review_pages table';
            $table->string('title', 150)->comment = 'Star';
            $table->integer('helpful_count')->default(0)->comment = 'How many people found it helpful';
            $table->bigInteger('date')->comment = 'Review date timestamp';
            $table->text('body')->comment = 'Review Content';
            $table->string('reviewer', 100)->comment = 'Reviewer name';
            $table->string('review_id', 50)->comment = 'Review id';
            $table->integer('verified_purchase')->comment = 'Is purchase verified';
            $table->integer('videos_count')->comment = 'Number of videos';
            $table->integer('images_count')->comment = 'Number of images';
            $table->string('badges', 250)->comment = 'Extra badge';

            $table->string('type', 50)->comment = 'positive, critical, normal';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('amazon_reviews');
    }
}
