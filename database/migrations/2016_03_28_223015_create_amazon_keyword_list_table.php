<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmazonKeywordListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_keyword_list', function (Blueprint $table) {
            $table->increments('id');

            $table->string('keyword', 50)->comment = "search term";
            $table->string('marketplace_id', 20)->nullable()->comment = "country";

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('amazon_keyword_list');
    }
}
