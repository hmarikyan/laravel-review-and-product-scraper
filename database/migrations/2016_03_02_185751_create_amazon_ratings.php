<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmazonRatings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amazon_page_id')->comment = 'Refers to amazon_review_pages table';
            $table->integer('star')->comment = 'Star';
            $table->double('percentage')->comment = 'Percentage';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('amazon_ratings');
    }
}
