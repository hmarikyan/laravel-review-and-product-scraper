<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmazonProductPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_product_pages', function (Blueprint $table) {
            $table->increments('id');

            $table->string('suggested_category', 150)->comment = 'Category';
            $table->text('suggested_keywords')->default('')->comment = 'Keywords';
            $table->string('result_shown', 50)->default('0')->comment = 'Products count on page';
            $table->integer('result_total')->default(0)->comment = 'Products total count';
            $table->string('seed_keywords', 200)->default('')->comment = 'Seed Keywords';

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('amazon_product_pages');
    }
}
