<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmazonAsinListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_asin_list', function (Blueprint $table) {
            $table->increments('id');

            $table->string('asin', 50)->comment = "Amazon product identifier";
            $table->string('marketplace_id', 20)->nullable()->comment = "country";
            $table->string('last_full_review_run', 30)->nullable()->comment = "tiestamp";

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('amazon_asin_list');
    }
}
