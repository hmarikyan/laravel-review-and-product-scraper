[TOC]

----------------------------

# Parameters
* asins - comma separated list of asins. Up to 20  
* dataTypes - comma separated list of data types  

----------------------------
#Constants:
REVIEW_RETURN_COUNT = 20

----------------------------
#Data Types:
* overview - this will contain the number of reviews and the average review    
* breakdown - this will return an array containing the number of reviews, and percentages of reviews for each star rating
* top - this will return the critical and positive reviews
* reviews:0 - this will return a number of reviews equal to REVIEW_RETURN_COUNT starting at 0, ranked by the review  creation date (not the date we created it in the database). We should take(self::REVIEW_RETURN_COUNT) and skip(0) in this case. We should be able to pass any number we want here and have it skip it. So if we passed in reviews:100, it should skip(100) in the query.

----------------------------
#Other:
All of the database queries should use the query builder instead of eloquent. We have seen a 10-15x speed improvement in using this over eloquent. In an API like this, speed will be very important!

----------------------------
#Response Structure
We will eventually return a JSON response. We will build the response as an array (or object, if you prefer). Each asin will have it's own group of data types.

```
#!php

$response = [
    'TESTASIN01' => [
        //TESTASIN01 data
    ],
    'TESTASIN02' => [
        //TESTASIN02 data
    ]
];
```

----------------------------
# Data Types
## Overview
Response should be structured like this

```
#!php

$response = [
    'TESTASIN01' => [
        //TESTASIN01 data
    ],
    'TESTASIN02' => [
        //TESTASIN02 data
    ]
];
```

## Breakdown
Response should be structure like this

```
#!php

 'breakdown' => [
    'count' => [
            'stars_5' => 600,
            'stars_4' => 200,
            'stars_3' => 100,
            'stars_2' => 75,
            'stars_1' => 25
        ],
    'percentage' => [
        'stars_5' => 60,
        'stars_4' => 20,
        'stars_3' => 10,
        'stars_2' => 8,
        'stars_1' => 3,
    ]
 ]
```

## Top
Response should be structured like this

```
#!php

'top' => [
            'positive' => [
                // Review response
            ],
            'negative' => [
                // Review response
            ]
        ],
```

## Reviews
This will take an integer as a parameter. Format will be reviews:0 or reviews:20, reviews:60, etc.


```
#!php

 'reviews' => [
            // review response,
            // review response,
            // review response
        ]
```

----------------------------
# Sample inputs and outputs
** Input **

```
#!php

$asins = 'TESTASIN01,TESTASIN02';
$dataTypes = 'overview,breakdown,top,reviews:20'
```

** Output **

```
#!php

$response = [
    'TESTASIN01' => [
        'overview' => [
            'average_review' => 4.0,
            'total_reviews' => 1000
        ],
        'breakdown' => [
    'count' => [
            'stars_5' => 600,
            'stars_4' => 200,
            'stars_3' => 100,
            'stars_2' => 75,
            'stars_1' => 25
        ],
    'percentage' => [
        'stars_5' => 60,
        'stars_4' => 20,
        'stars_3' => 10,
        'stars_2' => 8,
        'stars_1' => 3,
    ]
 ]
        'top' => [
            'positive' => [
                // Review response
            ],
            'negative' => [
                // Review response
            ]
        ],
        'reviews' => [
            // review response,
            // review response,
            // review response
        ]
    ],
    'TESTASIN02' => [
        'overview' => [
            'average_review' => 4.0,
            'total_reviews' => 1000
        ],
        'breakdown' => [
            'count' => [
                'stars_5' => 600,
                'stars_4' => 200,
                'stars_3' => 100,
                'stars_2' => 75,
                'stars_1' => 25
            ],
            'percentage' => [
                'stars_5' => 60,
                'stars_4' => 20,
                'stars_3' => 10,
                'stars_2' => 8,
                'stars_1' => 3,
            ]
        ]
        'top' => [
            'positive' => [
                // Review response
            ],
            'negative' => [
                // Review response
            ]
        ],
        'reviews' => [
            // review response,
            // review response,
            // review response
        ]
    ]
];
```