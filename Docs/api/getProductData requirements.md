
[TOC]

----------------------------

# Parameters
* asins - comma separated list of asins. Up to 20  
* dataTypes - comma separated list of data types 
* pastDays - optional parameter. Will determine how far back we will look for results. Default of 7

----------------------------
#Constants:


----------------------------
#Data Types:
* normal - this will a list of keywords in which the given asins have showed up for in normal results
* ppc - this will a list of keywords in which the given asins have showed up in the sponsored sections
----------------------------
#Other:
All of the database queries should use the query builder instead of eloquent. We have seen a 10-15x speed improvement in using this over eloquent. In an API like this, speed will be very important!

----------------------------
#Response Structure
We will eventually return a JSON response. We will build the response as an array (or object, if you prefer). Each asin will have it's own group of data types.

```
#!php

$response = [
    'TESTASIN01' => [
        //TESTASIN01 data
    ],
    'TESTASIN02' => [
        //TESTASIN02 data
    ]
];
```

----------------------------
# Data Types
## Normal
Response should be structured like this

```
#!php

$response = [
    'TESTASIN01' => [
        'normal' => [
              'garlic press',
              'world's best garlic press',
              'great garlic press'
        ]
    ]
];
```

## PPC
Response should be structure like this

```
#!php

$response = [
    'TESTASIN01' => [
        'ppc' => [
              '#1 garlic press',
              'world's super garlic press',
              'awesome garlic press'
        ]
    ]
];
```

----------------------------
# Sample inputs and outputs
** Input **

```
#!php

$asins = 'TESTASIN01';
$dataTypes = 'normal,ppc'
```

** Output **

```
#!php

$response = [
    'TESTASIN01' => [
        'normal' => [
              'garlic press',
              'world's best garlic press',
              'great garlic press'
        ],
        'ppc' => [
              '#1 garlic press',
              'world's super garlic press',
              'awesome garlic press'
        ]
    ]
];
```