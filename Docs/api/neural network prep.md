# Overview
This will be fun :) Here we will be laying the ground work to do some machine learning.

What we are trying to do is to determine whether or not a review is a "real" review. Some sellers will sell products for $0 if the buyer leaves a review. Amazon knows this happens and requires those buyers to include a disclaimer that is something along the lines of "I received this product at a discount in exchange for an honest review"

We will be building a system where we can input a review as a string, and get a boolean back as to whether or not that review contains the disclaimer.

So "Awesome product! Really great! I would recommend to all my friends. I got a discount on this product in exchange for an honest review" would return true. And "Really great product!" would return false.

# Requirements
Machine learning requires a large set of data to learn on. This data needs known values in order to be trained correctly.

So we will be training it with about 100k different reviews with all sorts of different review variations. We will be using mturk to have people do the initial classification of reviews. And we need to build a system to do that.

Work in progress. More to come later!