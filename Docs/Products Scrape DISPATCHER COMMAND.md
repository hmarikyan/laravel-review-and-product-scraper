# Products Scrape QUEUE DISPATCHER COMMAND

Class: ```App\Console\Commands\DispatchProductScrapeJobs```

Command by cli: ```php artisan scrapper:amazon_product```

Scheduler: At 03:00 ```$schedule->command('scrapper:amazon_product')->dailyAt('03:00');```

---------------------------------------------------

## Purpose

This command dispatches all the product(search page) scrape jobs from amazon_keyword_list table(AmazonKeywordList model).


--------------------------------------------------------------------

## Flow

It gets keywords from AmazonKeywordList model, then For each record 

Checks whether the job should be dispatched or not ```App\Packages\ProductScrapeReadyCheck::check(AmazonKeywordList $model)```
Currently it returns 'true': 


Then it calls queue job:

```$job = (new SearchResultsPageScrapeJob($k->keyword))->onQueue('amazon_search_results_scrape');
   $this->dispatch($job);```



In order to listen the queue:

```php artisan queue:listen --queue=amazon_search_results_scrape```