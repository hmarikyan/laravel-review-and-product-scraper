#ReviewPage Scrape Queue Documentation

This satisfies [ReviewPage Requirement](https://bitbucket.org/tnmholdings/feedback-tool/wiki/ReviewScrapeRequirements)

-------------------------------
#Queue Main

Product Review job looks like following 

1. All review scrape
    Parameters: 
    * ```$asin``` - amazon product identifier  
    * ```$url = null``` - amazon url to scrape, This is used for adding pagination queues
    * ```$page_number = null``` - pagination current page, used to track and use pageination limitation, 
                                  Currently it's 5

    ```$job = (new ReviewsPageScrapeAllJob($asin, $url = null, $page_number = 1))->onQueue('amazon_scrape_reviews');```

2. Recent review scrape
    Parameters: 
    * See above 'All review scrape'
    * ```$most_recent_review_id``` - used to paginate with current most recent review

    ```$job = (new ReviewsPageScrapeRecentJob($asin, $url = null, $page_number = 1, $most_recent_review_id = null))->onQueue('amazon_scrape_reviews');```


Add new Job onto queue:
In controllers the following code is sufficient in order to add new queue.

```
#!php
use App\Jobs\ReviewsPageScrapeAllJob;
use App\Jobs\ReviewsPageScrapeRecentJob;

$job = (new ReviewsPageScrapeRecentJob("B0094KOSO6"))->onQueue('amazon_scrape_reviews');
$this->dispatch($job);
```

In any other places should be added 'DispatchesJobs' trait

```
#!php
use Illuminate\Foundation\Bus\DispatchesJobs;

class SomeClass{
   use DispatchesJobs;

   public function someFunction(){
       // dispatch queue job
   }
}
```
-------------------------------

# Handler

Uses ScrapeTrait, for getting amazon html page
Uses static constructor for building URLs.

In any case it scrapes page level data including positive/critical reviews.
Firstly scrapes page level data, then goes to 'normal' reviews, then checks 'next page available'/'pagination limit', and eventually add new queue job.
'Recent reviews' - if there is no any most recent review for that product, it scrapes all reviews with pagination.

---------------------------------

# Queue work/listen

In order to listen review queue : run the following command in command shell
``` php artisan queue:listen --queue=amazon_scrape_reviews ```

To run only the next queue job 
``` php artisan queue:work--queue=amazon_scrape_reviews ```