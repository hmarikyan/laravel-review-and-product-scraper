# Markdown Cheat Sheet

The repository wiki is the preferred place for any code related documentation. If you have never used a repository wiki or are unfamiliar with the markdown syntax, this guide is designed to help make that learning process easy!

---------------------------

# Table of Contents

[TOC]

---------------------------

#Text

```
# Big text
```
# Big text


```
## Smaller text
```
## Smaller text


```
### Still smaller text
```
### Still smaller text


```
#### Smallest text
```
#### Smallest text


```
Normal text
```
Normal text

---------------------

#Lists
```
* Unordered list 1
* Unordered list 2
* Unordered list 3
```
* Unordered list 1
* Unordered list 2
* Unordered list 3


```
1. First
2. Second
3. Third
```
1. First
2. Second
3. Third

To indent lists, just add four spaces before

1. First main category  
    * Subcategory 1  
    * Subcategory 2  
2. Second main category 

If there are ever any formatting errors, usually an extra return or two spaces at the end of the line fixes it

----------------------
# Horizontal Lines

Horizonal lines can be made by entering a series of dashes

```
---------------------
```

---------------------

# Links

Links are made by wrapping the link text in square brackets and the link url in parenthesis, with no spaces between. Like this
```
[best search engine!](google.com)
```
[best search engine!](google.com)

---------------------

# Images



---------------------

# Table of Contents

Just add
```
[TOC]
```
To create a table of contents

------------------------

# Code

Wrapping up code in three backticks (`) will format it. To get proper syntax highlighting, add the bash style thing (
```#!php```) to the beginning

```
#!php

$myPhpVariable = 'hello world!';
```

------------------------

# References

[https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](Markdown Cheatsheet)