# How to clone a repository

1. In terminal, navigate to the folder above your destination. If I want to install in ```/var/www/my-app-name```
, I would navigate to ```/var/www```

2. ```sudo composer create-project laravel/laravel price-and-rank-tracker "5.1.*"```
3. ```cd price-and-rank-tracker```
4. ```composer install```
5. ```composer update```
6. ```composer install```