# Reviews page scrapper

[TOC]

----------------------------------------------------------
## Intro

Class App\Packages\SuggestedSearchScrape\AmazonReviewScraper.php

Implements interface : use App\Packages\SuggestedSearchScrape\SuggestedSearchScrapeInterface; 
Uses  App\Packages\Repositories\AmazonReviewRepository;

------------------------------------------------------------------

## Basic usage

migrate database tables 
```php artisan migrate```

```
#!php
  //before class
  use App\Packages\AmazonReviewScraper;

  //In action
  $url = "http://www.amazon.com/Alpha-Grillers-Garlic-Stainless-Silicone/product-reviews/B00I937QEI/ref=cm_cr_arp_d_viewopt_srt?ie=UTF8&showViewpoints=1&sortBy=recent&pageNumber=1";

  $web_page = file_get_contents($url);
  $amazon_scrapper = new AmazonReviewScraper($web_page);
  $amazon_scrapper->set_url($url);


  $result = $amazon_scrapper->prepare()->save();
```

The result contains scrapped data : array(review_page, reviews, ratings)
As well as data is saved in database.

-----------------------------------------------------------------------------

## Sample

Made sample page for demonstrate scrapping -
type  '/scrap' in browser, insert url in 1st input, then hint 'scrap data', 
App will load amazon page , parse it accordingly , save in db and show scrapped data in ugly tables.


------------------------------------------------------------------------------------------------------

## Data parts

In order to get particular data parts from amazon page and not-save in db, can be called particular methods from AmazonReviewScraper class.

```
#!php

    $url = "http://some_amazon_url";
    $web_page = file_get_contents($url);
     
    $amazon_scrapper = new AmazonReviewScraper($web_page);
    $amazon_scrapper->set_url($url);

    //data parts
    $p =  $amazon_scrapper->get_review_page();
    $rt = $amazon_scrapper->get_ratings();
    $p_rw = $amazon_scrapper->get_positive_review();
    $c_rw = $amazon_scrapper->get_critical_review();
    $n_rw = $amazon_scrapper->get_normal_reviews();
```

Above data are array structure
If needed to save data into db , it can be done manually

Example.
```
#!php

 $p =  $amazon_scrapper->get_review_page();
 $product_model = new AmazonReviewPages(); 
 // 3 tables/models:  AmazonReviewPages, AmazonRatings,  AmazonReviews
 $product_model = AmazonReviewScraper::load_model_data($product_model, $p);
 $product_model->save();

```

Example Scrapped data:

```
#!php

Array
(
    [review_page] => Array
        (
            [title] => Alpha Grillers Garlic Press and Peeler Set. Stainless Steel Mincer and Silicone Tube Roller
            [average_rating] => 4.8
            [total_reviews] => 1135
            [url] => http://www.amazon.com/Alpha-Grillers-Garlic-Stainless-Silicone/product-reviews/B00I937QEI/ref=cm_cr_arp_d_viewopt_srt?ie=UTF8&showViewpoints=1&sortBy=recent&pageNumber=1
            [is_next_page] => 1
        )

    [ratings] => Array
        (
            [0] => Array
                (
                    [percentage] => 89
                    [star] => 5
                )

            [1] => Array
                (
                    [percentage] => 7
                    [star] => 4
                )

            ............................
            ............................
        )

    [reviews] => Array
        (
            [0] => Array
                (
                    [type] => normal
                    [title] => Super good garlic press!
                    [helpful_count] => 
                    [reviewer] =>  joey b. hall 
                    [date] => 1458777600
                    [body] => You know, sometimes you find a good, ....... 
                    [review_id] => R165T2FDXPI98R
                    [verified_purchase] => 1
                    [badges] => 
                    [videos_count] => 0
                    [images_count] => 0
                )

            [1] => Array
                (
                    [type] => normal
                    [title] => Must have for garlic lovers!
                    [helpful_count] => 
                    [reviewer] =>  maryann darnauer 
                    [date] => 1458691200
                    [body] => We LOVE this gadget! I highly recommend it! .....
                    [review_id] => R3HGO6EPCZV4HO
                    [verified_purchase] => 1
                    [badges] => 
                    [videos_count] => 0
                    [images_count] => 0
                )

            ............................
            ............................

            [10] => Array
                (
                    [type] => positive
                    [title] => OXO Good Grips versus Alpha Grillers Garlic Press
                    [helpful_count] => 39
                    [reviewer] =>  JP 
                    [date] => 1412553600
                    [body] => My old garlic press was an OXO Good Grips press that is still ....
                    [review_id] => R2108BVDWMG92V
                    [verified_purchase] => 0
                    [badges] => 
                    [videos_count] => 0
                    [images_count] => 0
                )

            [11] => Array
                (
                    [type] => critical
                    [title] => Works fine but hard to clean
                    [helpful_count] => 16
                    [reviewer] =>  Shane 
                    [date] => 1419724800
                    [body] => There is one critical flaw in this garlic press in my opinion.  ...... 
                    [review_id] => R31FGIPTM6TDEB
                    [verified_purchase] => 0
                    [badges] => 
                    [videos_count] => 0
                    [images_count] => 0
                )

        )

)
```

-------------------------------------------------------

## DB tables

3 tables in db for amazon reviews data
```
#!php
    Schema::create('amazon_review_pages', function (Blueprint $table) {
        $table->increments('id');
        $table->string('title', 250)->comment = 'Product name';
        $table->text('url', 250)->comment = 'Product url';
        $table->integer('total_reviews')->comment = 'Total eview count';
        $table->double('average_rating')->comment = 'Average review rating';
        $table->integer('is_next_page')->comment = 'Next page of reviews available';
        $table->timestamps();
    });
    
    Schema::create('amazon_ratings', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('amazon_page_id')->comment = 'Refers to amazon_review_pages table';
        $table->integer('star')->comment = 'Star';
        $table->double('percentage')->comment = 'Percentage';
        $table->timestamps();
   });
           
   Schema::create('amazon_reviews', function (Blueprint $table) {
       $table->increments('id');
       $table->integer('amazon_page_id')->comment = 'Refers to amazon_review_pages table';
       $table->string('title', 150)->comment = 'Star';
       $table->integer('helpful_count')->default(0)->comment = 'How many people found it helpful';
       $table->string('date',50)->comment = 'Review date';
       $table->text('body')->comment = 'Review Content';
       $table->string('reviewer', 100)->comment = 'Reviewer name';
       $table->string('review_id', 50)->comment = 'Review id';
       $table->integer('verified_purchase')->comment = 'Is purchase verified';
       $table->integer('videos_count')->comment = 'Number of videos';
       $table->integer('images_count')->comment = 'Number of images';
       $table->string('badges', 250)->comment = 'Extra badge';

       $table->string('type', 50)->comment = 'positive, critical, normal';
       $table->timestamps();
   });
```