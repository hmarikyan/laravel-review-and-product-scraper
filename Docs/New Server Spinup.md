# New Server Spinup

* sudo mysql_secure_installation (done already)  
* sudo apt-get update  
* sudo apt-get dist-upgrade  
* sudo apt-get install git  
* sudo a2enmod rewrite  
* sudo apt-get install phpmyadmin
       1. apache 2
       2. yes

* sudo vi /etc/apache2/apache2.conf
       1. add 'include /etc/phpmyadmin/apache.conf' to the end of the file
       2. add 'ServerName localhost' //if this wasn't setup previously
       3. service apache2 restart

# Installing composer
* curl -sS https://getcomposer.org/installer | php  
* sudo mv composer.phar /usr/local/bin/composer  
* You should be able to ```composer``` now and get a message  

# Initial code download
* cd /var/www  
* git clone the repository  
* cd into the newly created repository  
* composer install  
* chmod -R 777 storage
* if you neeed to run migration: 'php artisan migrate'
  
# Apache

This part tends to be a little troublesome

change DocumentRoot in 000-default.conf  
vi /etc/apache2/sites-enabled/000-default.conf  
```
DocumentRoot /var/www/<directory name>/public
```

add directory in apache2.conf  
vi /etc/apache2/apache2.conf  
```
#!
<Directory /var/www/<directory name>/public>
    AllowOverride All
    Options Indexes FollowSymLinks
    Order allow,deny
    Allow from all
</Directory>

```
sudo service apache2 restart

# Redis Install

1. ```sudo add-apt-repository ppa:chris-lea/redis-server```
1. ```sudo apt-get update```
1. ```sudo apt-get install redis-server```
1. ```redis-benchmark -q -n 1000 -c 10 -P 5```

```vi /etc/redis/redis.conf```

If local only,  
bind 127.0.0.1

Add a password  
requirepass your_redis_master_password

Save & exit


```
#!

sudo service redis-server restart
```


Sources:
Just step 1 in this page

[helpful digital ocean article](https://www.digitalocean.com/community/tutorials/how-to-configure-a-redis-cluster-on-ubuntu-14-04)

[https://www.digitalocean.com/community/tutorials/how-to-secure-your-redis-installation-on-ubuntu-14-04](https://www.digitalocean.com/community/tutorials/how-to-secure-your-redis-installation-on-ubuntu-14-04)

You should have a password that was sent to you that you should use

# .env setup

By default, the .env is ignored from the git repository. So we will have to create one or Laravel will throw an error. The easiest way to do this is to just copy the one you have been using for local development.

'sudo nano .env'

From there, you will just need to change the database information to the correct ones and you should be able to connect.