# Search page scrapper

[TOC]

----------------------------------------------------------
## Intro

Class App\Packages\SuggestedSearchScrape\AmazonProductScraper.php

Implements interface : use App\Packages\SuggestedSearchScrape\SuggestedSearchScrapeInterface; 

Uses  App\Packages\Repositories\AmazonProductRepository;

------------------------------------------------------------------

## Basic usage

```
#!php
  //before class
  use App\Packages\AmazonProductScraper;

  //In action
  $url = "search url";

  $web_page = file_get_contents($url);
 
  $amazon_scrapper = new AmazonProductScraper(null, $web_page);
  $amazon_scrapper->set_url($url);

  $data = array();

  $result = $amazon_scrapper->prepare()->save();

```

The result contains scrapped data : array(product_page, products, ad)
As well as data is saved in database.

-----------------------------------------------------------------------------

## Sample

Type  /scrap in browser, insert url in 2nd input, then click 'scrap data',

------------------------------------------------------------------------------------------------------

## Data parts

In order to get particular data parts from amazon page and not-save in db, can be called particular methods from AmazonReviewScraper class.

```
#!php

   $url = "http://some_amazon_url";
   $web_page = file_get_contents($url);
    
   $amazon_scrapper = new AmazonProductScraper($web_page);
   $amazon_scrapper->set_url($url);
   
    //data parts
    $ad =  $amazon_scrapper->get_ad();
    $pp = $amazon_scrapper->get_page();
    $p = $amazon_scrapper->get_products();

```

Above data(s) are arrays, If needs to save data into db , it can be done manually
Example.
```
#!php

 $pp =  $amazon_scrapper->get_page();
     $page_model = new AmazonProductPages(); 
     // 3 tables/models:  AmazonProductPages, AmazonProducts,  AmazonAds
     $page_model = AmazonProductScraper::load_model_data($page_model, $pp);
     $page_model -> save();

```

Example Scrapped data

```
#!php 

Array
(
    [product_page] => Array
        (
            [result_shown] => 1-24
            [result_total] =>  1532 
            [suggested_category] => Home & Kitchen
            [suggested_keywords] => garlic mincer
            [seed_keywords] => custom keys
        )

    [ad] => Array
        (
            [link] => http://aax-us-east.amazon-adsystem.com/x/c/QQ6TmaRqcA2yJRg07JP13GMAAAFTrwXRzQEAAAH2lPY8cQ///www.amazon.com/uberchef
            [title] => Welcome To Your Improved Kitchen...
            [brand] => UberChef
        )

    [products] => Array
        (
            [0] => Array
                (
                    [asin] => B00Z4GTD8U
                    [is_sponsored] => 0
                    [title] => Stainless Steel Garlic Press by Kitchen Fanatic, with Silicone Garlic Peeler, Ergonomic, Pro-Quality
                    [brand] =>  Kitchen Fanatic 
                    [current_price] => 11.97
                    [original_price] => 24.95
                    [is_prime] => 1
                    [review_avg] => 4.7
                    [review_count] => 1175
                    [best_seller] => #1  Best Seller in Garlic Presses  
                    [rank] => 1
                    [promotions_details] => 
                    [promotions] => 0
                    [offers] => 
                    [variations] => 
                    [type] => normal
                )
        
            [1] => Array
                (
                    [asin] => B00NUIN92K
                    [is_sponsored] => 0
                    [title] => UberChef&reg; Premium Stainless Steel Garlic Press &amp; Peeler Set &#9679; Mince &amp; Crush Garlic Cloves &amp; Ginger with Ease &#9679;...
                    [brand] =>  UberChef 
                    [current_price] => 14.95
                    [original_price] => 32.95
                    [is_prime] => 1
                    [review_avg] => 4.8
                    [review_count] => 885
                    [best_seller] => 
                    [rank] => 8
                    [promotions_details] => 15% off item with purchase of 1 items , 15% off item with purchase of 1 items , 15% off coupon with purchase of 1 items , 15% off coupon with purchase of 1 items , 
                    [promotions] => 4
                    [offers] => 
                    [variations] => 
                    [type] => normal
                )           

            [2] => Array
                (
                    [asin] => B01ATV4O2O
                    [is_sponsored] => 0
                    [title] => Joseph Joseph 20037 Garlic Rocker, Stainless Steel
                    [brand] =>  Unknown 
                    [current_price] => 17.95
                    [original_price] => 18.00
                    [is_prime] => 0
                    [review_avg] => 4.1
                    [review_count] => 709
                    [best_seller] => 
                    [rank] => 10
                    [promotions_details] => 
                    [promotions] => 0
                    [offers] => $17.19  new (2 offers), 
                    [variations] => See Style & Color Options
                    [type] => normal
                )
            [3] => Array
                (
                    [asin] => B0143IY85W
                    [is_sponsored] => 0
                    [title] => Krytonyx Garlic Press, UNIQUE 2 in 1 Design with Swap Slicer and FREE Garlic Peeler to Mince Cloves as a Garlic...
                    [brand] =>  Krytonyx Exquisite Engineering 
                    [current_price] => 13.95
                    [original_price] => 39.99
                    [is_prime] => 1
                    [review_avg] => 4.4
                    [review_count] => 68
                    [best_seller] => 
                    [rank] => 18
                    [promotions_details] => 25% off purchase of 3 items , 
                    [promotions] => 1
                    [offers] => 
                    [variations] => 
                    [type] => normal
                )
                
             ........................................
             ........................................
        )
)

```

-------------------------------------------------------

## DB tables

3 tables in db for amazon search products data
```
#!php
     Schema::create('amazon_product_pages', function (Blueprint $table) {
        $table->increments('id');

        $table->string('suggested_category', 150)->comment = 'Category';
        $table->text('suggested_keywords')->default('')->comment = 'Keywords';
        $table->string('result_shown', 50)->default('0')->comment = 'Products count on page';
        $table->integer('result_total')->default(0)->comment = 'Products total count';
        $table->string('seed_keywords', 200)->default('')->comment = 'Seed Keywords';

        $table->timestamps();
    });
    
    Schema::create('amazon_products', function (Blueprint $table) {
        $table->increments('id');

        $table->integer('amazon_product_page_id')->comment = 'Refers to amazon_product_pages table';

        $table->string('asin', 100)->comment = 'ASIN code';
        $table->string('rank', 50)->comment = "Result rank";
        $table->integer('is_sponsored')->comment = 'Sponsored or not';
        $table->integer('is_prime')->comment = 'Prime eligible';

        $table->string('title', 150)->comment = 'Product title';
        $table->string('brand', 100)->comment = 'Product brand';

        $table->double('current_price');
        $table->double('original_price');

        $table->double('review_avg')->comment = "Reviews cca value";
        $table->integer('review_count')->comment = "Reviews quantity";

        $table->text('best_seller')->comment = "Best seller badges";
        $table->integer('promotions')->default(0)->comment = "Promotions count";
        $table->text('promotions_details')->comment = "Promotions content";

        $table->text('offers')->comment = "Other offers";
        $table->string('variations', 250)->default(0)->comment = "variations : See Color Options, See more choices ";

        $table->string('type')->default('normal')->comment = "Product type sponsored:normal";

        $table->timestamps();
    });
            
     Schema::create('amazon_ads', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('amazon_product_page_id')->comment = 'Refers to amazon_product_pages table';

        $table->string('title', 200)->comment = 'Ad title';
        $table->text('link')->default('');
        $table->string('brand',100)->comment = 'Advertizer';

        $table->timestamps();
    });
```
