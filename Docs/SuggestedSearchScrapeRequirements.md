# Suggested Search Scrape Requirements

**Who:** This will be primarily used by the backend. It will be "kicked off" from a user submission.  
**What:** It will take a seed keyword and search engine(s). Then it will exhaustively find a list of suggested keywords for all of the seed keywords and any results.   
**Why:** Used to create a database of related keywords. This will help sellers find additional high volume search terms that they would not have otherwise thought to use.  

-------------------------

#Overview

Basic flow:

![Suggested Search Scrape.png](https://bitbucket.org/repo/8GLezj/images/1082177171-Suggested%20Search%20Scrape.png)

This will be the next step of the suggested keyword scrape project. What we will be doing here is tying all the existing pieces together into a tool that's ready for production!

We will be using both the queue and job systems a LOT for this section. 

**The goal is to do an exhaustive search for a given seed keyword. So we will be searching until we have found all the keywords that show up for a suggested keyword.**

So lets say the user enters "tv" as a seed keyword. The program should take that term, run it through the selected search engines, and save the results. It should also look at the results and add those to the queue of keywords that need to be run.

"tv" -> "sony tv, vizio tv, best buy tv, walmart tv, 60" tv"  
"sony tv" -> ...  
"vizio tv" -> ...  
"best buy tv" -> ...  
etc

For broader terms like "tv" we may end up with a hundred thousand or more searches. That is okay and what we are looking for. For testing, it will be much easier to work with smaller keywords. 

A little more on the "add additional keywords" box:

What this should do is check if the number of keywords found is equal to the max number of search results for that search engine. If so, then we want to add the a-z search terms for that keyword.

So if "tv" was searched on Amazon and it returned the max number of results for Amazon (12, I think), then we want to add the following keywords to the queue to search.

tv a  
tv b  
tv c  
tv d  
...  
tv x  
tv y  
tv z  
a tv  
b tv  
c tv  
...  
y tv  
z tv  

Some pseudocode:

```
if ($countResults == $maxResults) {

    foreach(range('a','z'), as $letter) {

        Queue::pushOn('keyword_queue', new KeywordJob($this->user_id, $this->job_id, $keyword . ' ' . $letter);
        Queue::pushOn('keyword_queue', new KeywordJob($this->user_id, $this->job_id, $letter . ' ' . $keyword);

    }
}
```

-----------------------------------

# Completion

When done, we should be able to
```
Queue::pushOn('keyword_queue', new KeywordJob($this->user_id, $this->job_id, $seedKeyword);
```

and have it search until it can't find any more keywords.

----------------------------------

# Classes

## SuggestedSearchScrapeHandler

** Overview **  

* will be used to handle the underlying logic of the suggested search scrape this includes
    * scraping the data
    * determining whether or not to add additional keywords
    * dispatching additional jobs onto the queue
* It should have a static constructor
* The function to get the HMTL source should be curl based

** Input **  

* keyword - the keyword to search
* jobId - the id of the current job. When a user submits a search request, we will generate a jobId which will be the same for any searches.
* userId
* searchEngine

## KeywordExpander

** Overview **

* This one already exists. It's located at App\Packages\SuggestedSearchScrape\KeywordExpander
* It's a pretty simple class. Usage will be somewhat like this
    1. ```$keywords = ['first','second','third'];```
    1. ```KeywordExpander::make($keywords)->setMaxKeywords(10)->setSeedKeyword('seed')->getKeywords();```
* It will return an array of keywords. If we are at the maximum number of keyword for the given search engine, then we will add additional keywords to the result.

-----------------------------

# Jobs

These should be created using the ```php artisan make:job KeywordJob --queued``` command
The jobs themselves should have minimal logic in them. Most of the logic should be in the handler.

## KeywordJob

** Overview **

* Will be used to scrape an individual keyword from the search engine, save the results, and dispatch new jobs onto the queue

** Input ** 

* keyword - keyword to search
* jobId
* userId
* searchEngine

------------------------------

# Example Run

This section attempts to show what is happening on each "iteration" of the scrape.  
The keyword list represents a list of all the found keywords so far.  
The suggested results are the suggested keyword results for the given search term.  

** First Search **

search term: dog leash  

suggested results from Amazon:  
best dog leash  
nylon dog leash  

keyword list:  
dog leash  
best dog leash  
nylon dog leash  

------------------------

** Second Search **  
search term: best dog leash  

suggested results form Amazon:  
best pink dog leash  
best locking dog leash  

keyword list:  
dog leash  
best dog leash  
nylon dog leash  
best pink dog leash  
best locking dog leash  

-----------------------

** Third Search **  
search term: best pink dog leash  

suggested results form Amazon:  
(none)  

keyword list:  
dog leash  
best dog leash  
nylon dog leash  
best pink dog leash  
best locking dog leash  

-----------------------

** Fourth Search **  
search term: best locking dog leash  

suggested results form Amazon:  
best locking dog leash nylon  

keyword list:  
dog leash  
best dog leash  
nylon dog leash  
best pink dog leash  
best locking dog leash  
best locking dog leash nylon  

-----------------------

** Fifth Search **  
search term: best locking dog leash nylon  

suggested results form Amazon:  
(none)  

keyword list:  
dog leash  
best dog leash  
nylon dog leash  
best pink dog leash  
best locking dog leash  
best locking dog leash nylon  

-----------------------

** Sixth Search **  
search term: nylon dog leash  

suggested results form Amazon:  
strong nylon dog leash  

keyword list:  
dog leash  
best dog leash  
nylon dog leash  
best pink dog leash  
best locking dog leash  
best locking dog leash nylon  
strong nylon dog leash  

-----------------------

** Seventh Search **  
search term: strong nylon dog leash

suggested results form Amazon:  
(none)

keyword list:  
dog leash  
best dog leash  
nylon dog leash  
best pink dog leash  
best locking dog leash  
best locking dog leash nylon  
strong nylon dog leash  