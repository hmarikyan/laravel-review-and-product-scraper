#Review Page Scrape Requirements

**Who**: This will be used by the backend service  
**What**: It will take a product's ASIN and scrape either just the new review data or all review data  
**Why**: Used to create a database of review changes over time. This will be used to show new reviews, reviews that got deleted, how many reviews get added per day, etc.  

-----------------------
#Scenarios
Two major scenarios we will have to take into account

1. Scrape all review data
  this will walkt through every page of reviews for a product and save the results
2. Scrape most recent review data
  this will look at our current most recent reivew data for that product.
  It will walk through additional pages until it finds a review with a date that is older than our most recent
-----------------------
#Completion
When done, we should be able to 
```
Queue::pushOn('search_results_scrape', new ReviewsPageScrapeAllJob('B003Y3AZSM'))
```
and have it run, pull all the review data, and save it to the db.


Likewise, we should be able to
```
Queue::pushOn('search_results_scrape', new ReviewsPageScrapeRecentJob('B003Y3AZSM'))
```
and have it run, pull only the recent review data, and save it to the db.

-----------------------
#Classes
###ReviewScrapeHandler
**Overview**

* will be used to handle the underlying logic of the scraping. this includes:  
* how to paginate  
    * what to do, single vs all review scrape  
* It should have a static constructor
* The function to get the html source should be curl based

**Input**:

* asin - the asin of the product to search 
* searchAll - boolean, true/false 
* mostRecentReview - optional, model containing the date of the most recent review

-----------------------
#Jobs
These should be created using the 
```
php artisan make:job ReviewsPageScrapeAllJob --queued
``` command  
The jobs themselves should have minimal logic in them. Most of the logic should be in the Handler


###ReviewsPageScrapeAllJob
**Overview**

* will be used to run a "all" command to the ReviewScrapeHandler

**Input**:  

* asin - the asin of the product to search 



###ReviewsPageScrapeRecentJob
**Overview**

* Will be used to run a "recent" command to the ReviewScrapeHandler
* The most recent review should be pulled from the database here and passed into the ReviewScrapeHandler

**Input**:  

* asin - the asin of the product to search 