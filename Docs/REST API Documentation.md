# REST API

[TOC]

----------------------------------------------------

## Intro

Used Route group for all API methods

```
#!php
Route::group(['middleware' => 'auth.api', 'prefix' => '/api', 'namespace' => 'Api'], function () {
    Route::post('/asin', 'CommonController@insert_asin');
    
    //..........
});

```

Above code means api routes are available by url starting ```/api/*``` , 
Example: ```/api/asin```


Used middleware:

```auth.api => App\Http\Middleware\AuthApiMiddleware```

This middle validates http request's api_token with api_token from .env file, then passes to api controllers.

Variable in .env file: ```API_TOKEN=some_api_token```

In simple words, this an authorization functionality.

All requests should have ```api_token``` - either GET or POST parameter.

Response type is json.

Example responses of missing api token

```
// missing api token
//STATUS CODE 401
{
    "status" : false, 
    "error" : "Unauthorized: Missing API Token"
}

//STATUS CODE 401
// api token missmatch
{
    "status" : false,
    "error" : "Unauthorized: API Token doesn't match!"
}

```

Used Namespace:

All controllers should be in ```App\Http\Controllers\Api``` namespace.


-------------------------------------------------------------

## Asin route
    
* Method : POST
* Controller: Api\CommonController, action : insert_asin
* URL : /api/asin
* Parameters: 
     * api_token: string, required
     * asin: string, unique in amazon_asin_list table, required 
     * marketplace_id: string 
    

* Response

```
// ok/created response
//STATUS CODE 200
{
    "status" : true,
    "model" : {
        "asin" : "some_amazon_asin",
        "marketplace_id" : "us",
        "updated_at" : "2016-03-30 22:47:47",
        "created_at" : "2016-03-30 22:47:47",
        "id" : 5
    }
}

// bad request's response - input params validation error
//STATUS CODE 400
{
    "status" : false,
    "error" : "[\"The asin has already been taken.\"]"
}

// Internal server error - sql
//STATUS CODE 500
{
    "status" : false,
    "error" : "SQLSTATE[23000]: Integrity constraint violation: 1048 Column 'asin' cannot be null (SQL: insert into `amazon_asin_list` (`asin`, `marketplace_id`, `updated_at`, `created_at`) values (, us, 2016-03-30 23:02:06, 2016-03-30 23:02:06))"
}

```

Screenshot of phpstorm rest client.

![ad9cb4c1ecfdd0661f93cdbb6576aff1.png](https://bitbucket.org/repo/8GLezj/images/976467676-ad9cb4c1ecfdd0661f93cdbb6576aff1.png)


-----------------------------------
  
## Keyword route 
  
* Method : POST
* URL : /api/keyword
* Controller: Api\CommonController, action: insert_keyword
* Parameters: 
    * api_token: string, required
    * keyword: string, unique in amazon_keyword_list table, required 
    * marketplace_id: string 
  

* Response

```
// ok/created response
//STATUS CODE 200
{
    "status" : true,
    "model": {
        "keyword" : "dishes",
        "marketplace_id" : "us",
        "updated_at" : "2016-03-30 23:14:15",
        "created_at" : "2016-03-30 23:14:15",
        "id" : 2
    }
}

// bad request's response - input params validation error
//STATUS CODE 400
{
  "status" : false,
  "error" : "[\"The keyword has already been taken.\"]" //validation can have multiple messages for each field, therefore it's an array
}

// Internal server error - sql
//STATUS CODE 500
{
  "status" : false,
  "error" : "SQLSTATE[23000]: ..............."
}

```

-----------------------------------
  
## get Review Data Route

* Method : POST
* URL : /api/get_review_data
* Controller: Api\ReviewController, action: get_review_data
* Parameters: 
    * api_token: string, required,
    * asins: required, string, 
        example: "asin1, asin2"
    * data_types: required, string, 
        example: "overview,breakdown,top,reviews:0"
  
* Response

```
{
  "status": true,
  "results": {
    "B000GAYQT0": {
      "error": "No such data for asin:B000GAYQT0" // wrong asin 
    },
    "B00QAY4C4U": {
      "overview": {
        "id": 12,
        "url": "http:\/\/www.amazon.com\/Fanmis-S-Shock-Function-Resistant-Electronic\/product-reviews\/B00QAY4C4U\/ref=cm_cr_arp_d_paging_btm_5?ie=UTF8&pageNumber=5&sortBy=recent",
        "title": "Fanmis S-Shock Multi Function Digital LED Quartz Watch Water Resistant Electronic Sport Watches Blue",
        "average_rating": 4.3,
        "total_reviews": 473
      },
      "breakdown": {
        "percentage": {
          "stars_1": 8,
          "stars_2": 2,
          "stars_3": 6,
          "stars_4": 19,
          "stars_5": 65
        },
        "count": {
          "stars_1": 38,
          "stars_2": 9,
          "stars_3": 28,
          "stars_4": 90,
          "stars_5": 307
        }
      },
      "top": {
        "critical": {
          "id": 129,
          "amazon_page_id": 12,
          "title": "Inoperable Compass",
          "helpful_count": 18,
          "date": "1433548800",
          "body": "I wanted an inexpensive watch for hiking and camping.  I thought it was a bonus that this watch had a compass on its face.  ...... ",
          "reviewer": " Robert Dixon ",
          "review_id": "R9O2PKLTJXDW7",
          "verified_purchase": 0,
          "videos_count": 0,
          "images_count": 0,
          "badges": "",
          "type": "critical",
          "created_at": "2016-03-31 23:05:11",
          "updated_at": "2016-03-31 23:05:11"
        },
        "positive": {
          "id": 128,
          "amazon_page_id": 12,
          "title": "Sturdy, water resistant, changes colors & fast shipping!",
          "helpful_count": 101,
          "date": "1446076800",
          "body": "I love this watch, it's durable looks good and changes colors! .......",
          "reviewer": " Glendaly ",
          "review_id": "RO7I6XO7F6C3L",
          "verified_purchase": 0,
          "videos_count": 0,
          "images_count": 0,
          "badges": "",
          "type": "positive",
          "created_at": "2016-03-31 23:05:11",
          "updated_at": "2016-03-31 23:05:11"
        }
      },
      "reviews": [
        {
          "id": 130,
          "amazon_page_id": 12,
          "title": "Five Stars",
          "helpful_count": 0,
          "date": "1457827200",
          "body": "Very good price !! ",
          "reviewer": " Elix Hernandez ",
          "review_id": "R21TN9RWD7PRV0",
          "verified_purchase": 1,
          "videos_count": 0,
          "images_count": 0,
          "badges": "",
          "type": "normal",
          "created_at": "2016-03-31 23:05:11",
          "updated_at": "2016-03-31 23:05:11"
        },
        {
          "id": 131,
          "amazon_page_id": 12,
          "title": "Five Stars",
          "helpful_count": 0,
          "date": "1457740800",
          "body": "Excellent!!!! ",
          "reviewer": " Amazon Customer ",
          "review_id": "R15DSNZE60ASZS",
          "verified_purchase": 1,
          "videos_count": 0,
          "images_count": 0,
          "badges": "",
          "type": "normal",
          "created_at": "2016-03-31 23:05:11",
          "updated_at": "2016-03-31 23:05:11"
        },
        {
          "id": 132,
          "amazon_page_id": 12,
          "title": "battery died in 5 days after getting it. Felt ...",
          "helpful_count": 0,
          "date": "1457740800",
          "body": "battery died in 5 days after getting it. Felt extremely cheap. I got what I paid for and returned for my money back ",
          "reviewer": " Daniel A Guernick ",
          "review_id": "R2F0HZTRRZEDJ1",
          "verified_purchase": 1,
          "videos_count": 0,
          "images_count": 0,
          "badges": "",
          "type": "normal",
          "created_at": "2016-03-31 23:05:11",
          "updated_at": "2016-03-31 23:05:11"
        },
        
        ..................................
      ]
    },
    "B013Y720LM": {
      "error": "No such data for asin:B013Y720LM" // wrong asin  
    }
  }
} 
```

-----------------------------------------------------------

## get Product keyword Data Route

* Method : POST
* URL : /api/get_product_keyword_data
* Controller: Api\ProductController, action: get_product_keyword_data
* Parameters: 
    * api_token: string, required,
    * asins: required, string, 
        example: "asin1, asin2"
    * data_types: required, string, 
        example: "ppc,normal"
  
* Response

```
{
  "status": true,
  "results": {
    "B000GAYQT0": {
      "ppc": [],
      "normal": [
        "watch"
      ]
    },
    "B00QAY4C4U": {
         "error": "No such data for asin:B00QAY4C4U" // wrong asin 
    },
    "B013Y720LM": {
      "ppc": [
        "nice watch",
        "great watch"
      ],
      "normal": [
        "great watch"
      ]
    }
  }
}
```