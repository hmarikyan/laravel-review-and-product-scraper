#Search Results Scrape Requirements

**Who**: This will be used by the backend  
**What**: It will take a search term and scrape the relevant data from that  
**Why**: The scarped data will be used to see how products change their rank over time. Which new products are doing well, which old products are losing their market share, etc.  

-----------------------
#Scenarios
The most important thing we will have to account for here is Amazon's different page layouts. It seems like we have a great start on that already.

-----------------------
#Completion
When done, we should be able to 
```
Queue::pushOn('search_results_scrape', new SearchResultsPageScrapeJob('garlic press'))
```
and have it run, pull all the data, and save it to the db.

-----------------------
#Classes
###SearchResultsScrapeHandler
**Overview**

* will be used to handle any logic needed for scraping that isn't already built into the scrapers
    * building the url from the given keyword
    * pulling the page source
    * etc
* It should have a static constructor
* The function to get the HTML source should be curl based

**Input**:

* keyword - the keyword to search

**Notes**:
Both this handler and the review handler will share a method which will get a given url's source via CURL. Instead of duplicating code, it would be better to have both handlers extend a ScrapeHandler which will contain the method to get the page's source.

-----------------------
#Jobs
These should be created using the 
```
php artisan make:job ReviewsPageScrapeAllJob --queued
``` command  
The jobs themselves should have minimal logic in them. Most of the logic should be in the Handler

###SearchResultsPageScrapeJob
**Overview**

* will be used to queue search results scrapes.

**Input**:

* keyword - the keyword to search