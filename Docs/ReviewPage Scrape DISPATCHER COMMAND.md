# ReviewPage Scrape QUEUE DISPATCHER COMMAND

Class: ```App\Console\Commands\DispatchReviewScrapeJobs```

Command by cli: ```php artisan scrapper:amazon_review```

Scheduler: At 03:00 ```$schedule->command('scrapper:amazon_review')->dailyAt('03:00');```

---------------------------------------------------

## Purpose

This command dispatches all the review scrape jobs from amazon_asin_list table(AmazonAsinList model).

Dispatches jobs:
 
    * Daily : RECENT reviews
    * Weekly: ALL review data

--------------------------------------------------------------------

## Flow

It gets asin-s from AmazonAsinList model, then For each record 

Checks job type ```App\Packages\ReviewScrapeReadyCheck::check(AmazonAsinList $model)```
Static method is to determine which job should be dispatched/inserted into queue:
It returns: 

    * ReviewScrapeReadyCheck::ALL
    * ReviewScrapeReadyCheck::RECENT


Then it calls queue job:

```$job = (new ReviewsPageScrapeAllJob($asin->asin))->onQueue('amazon_scrape_reviews');```

or

```$job = (new ReviewsPageScrapeRecentJob($asin->asin))->onQueue('amazon_scrape_reviews');```

```$this->dispatch($job);```



In order to listen the queue:

```php artisan queue:listen --queue=amazon_scrape_reviews```