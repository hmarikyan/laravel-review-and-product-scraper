#SearchResult Page Scrape Queue Documentation

This satisfies [SearchResultPage Requirement](https://bitbucket.org/tnmholdings/feedback-tool/wiki/SearchResultsPageRequirements)

-------------------------------
#Queue Main

Product Scrape job looks like following 

Parameters:

*    ```$keyword``` - amazon search

``` $job = (new SearchResultsPageScrapeJob($keyword))->onQueue('amazon_search_results_scrape'); ```


Add new Job onto queue:
In controllers run the following code in order to add new queue.

```
#!php
use App\Jobs\SearchResultsPageScrapeJob;

 $job = (new SearchResultsPageScrapeJob("garlic"))->onQueue('amazon_search_results_scrape');
        $this->dispatch($job);
```

In any other places should be added 'DispatchesJobs' trait

```
#!php
use Illuminate\Foundation\Bus\DispatchesJobs;

class SomeClass{
   use DispatchesJobs;

   public function someFunction(){
       // dispatch queue job
   }
}
```
-------------------------------

# Handler

Uses ScrapeTrait, for getting amazon html page
Uses static constructor for building URLs.

Uses App\Packages\SuggestedSearchScrape\AmazonProductScraper class to scrape products and save data into database  

---------------------------------

# Queue work/listen

In order to listen products queue : run the following command in command shell
``` php artisan queue:listen --queue=amazon_search_results_scrape ```

To run only the next queue job 
``` php artisan queue:work--queue=amazon_search_results_scrape ```